package com.ebike.bean.station;

import java.util.ArrayList;

import com.ebike.bean.bike.Bike;

public class Station {

	private String id;
	private String name;
	private String address;
	private ArrayList<Bike> bikes;
	private int numberOfEmptyDocks;
	
	public Station() {
		super();
	}

	public Station(String id, String name, String address, ArrayList<Bike> bikes, int numberOfEmptyDocks) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.bikes = bikes;
		this.numberOfEmptyDocks = numberOfEmptyDocks;
	}



	public Station(String id, String name, String address) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
	}


	

	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}

	public void setBikes(ArrayList<Bike> bikes) {
		this.bikes = bikes;
	}
	
	public ArrayList<Bike> getBikes(){
		return this.bikes;
	}

	public int getNumberOfEmptyDocks() {
		return numberOfEmptyDocks;
	}

	public ArrayList<Bike> searchBikes(Bike bike) {
		ArrayList<Bike> rs = new ArrayList<Bike>();
		for (Bike b: bikes) {
			if (b.match(bike)) {
				rs.add(b);
			}
		}
		return rs;
	}
	public Bike searchBike(Bike bike) {
		return searchBikes(bike).get(0);
	}
	
	public void addBike(Bike bike) {
		this.bikes.add(bike);
	}

	public void setNumberOfEmptyDocks(int numberOfEmptyDocks) {
		this.numberOfEmptyDocks = numberOfEmptyDocks;
	}


//	public Bike rentBike(Bike bike) {
//		ArrayList<Bike> bikelist;
//		if(bike instanceof RegularBike) bikelist = this.bikes;
//		else
//	}

	public boolean match(Station t) {
		if (t == null)
			return true;
		if (t.id != null && !t.id.equals("") && !this.id.toLowerCase().contains(t.id.toLowerCase())) {
			return false;
		}

		if (t.name != null && !t.name.equals("") && !this.name.toLowerCase().contains(t.name.toLowerCase())) {
			return false;
		}
		if (t.address != null && !t.address.equals("") && !this.address.toLowerCase().contains(t.address.toLowerCase())) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Station) {
			return this.id.equals(((Station) obj).id);
		}
		return false;
	}

	@Override
	public String toString() {
		return "Station [id=" + id + ", name=" + name + ", address=" + address + ", bikes=" + bikes
				+ ", numberOfEmptyDocks=" + numberOfEmptyDocks + "]";
	}
	
	
}
