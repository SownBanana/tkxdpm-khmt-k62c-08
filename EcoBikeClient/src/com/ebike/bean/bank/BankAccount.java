package com.ebike.bean.bank;

public class BankAccount {

    private String cardHolderName;

    private String cardNumber;

    private String issuingBank;

    private String securityCode;

    private String expirationDate;

	public BankAccount() {
	}

	public BankAccount(String cardHolderName, String cardNumber, String issuingBank, String securityCode, String expirationDate) {
		this.cardHolderName = cardHolderName;
		this.cardNumber = cardNumber;
		this.issuingBank = issuingBank;
		this.securityCode = securityCode;
		this.expirationDate = expirationDate;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getIssuingBank() {
		return issuingBank;
	}

	public void setIssuingBank(String issuingBank) {
		this.issuingBank = issuingBank;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
}