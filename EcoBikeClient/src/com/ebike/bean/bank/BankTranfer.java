package com.ebike.bean.bank;

public class BankTranfer {

	private BankAccount accA;
	private double moneyTransfer;
	public BankAccount getAccA() {
		return accA;
	}
	public void setAccA(BankAccount accA) {
		this.accA = accA;
	}
	public double getMoneyTransfer() {
		return moneyTransfer;
	}
	public void setMoneyTransfer(double moneyTransfer) {
		this.moneyTransfer = moneyTransfer;
	}
}
