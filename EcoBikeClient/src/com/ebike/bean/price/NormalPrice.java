package com.ebike.bean.price;

import java.util.Date;

public class NormalPrice extends Price{
	private double firstPrice;
	private int firstDuration;
	private double stepPrice;
	private int stepDuration;

    public NormalPrice(double deposit, String bikeType, int freeFeeTime, double firstPrice, int firstDuration, double stepPrice, int stepDuration) {
        super(deposit, bikeType, freeFeeTime);
        this.firstPrice = firstPrice;
        this.firstDuration = firstDuration;
        this.stepPrice = stepPrice;
        this.stepDuration = stepDuration;
    }

    public NormalPrice() {
        super();
    }

    @Override
    public double calculatePrice(Date startDate, Date finishDate) {
		double rs=0;
		long minutes=(finishDate.getTime()-startDate.getTime())/(1000*60);
		if(minutes<0)
		    return 0;
		if(minutes>getFreeFeeTime()) {
            if (minutes <= firstDuration) rs = firstPrice;
            else {
                int step=(int)(minutes-firstDuration)/stepDuration;
                if(step*stepDuration<(minutes-firstDuration))
                    step++;
                rs = firstPrice + stepPrice * step;
            }
        }
		return rs;
	}

    @Override
    public Price createPrice(String bikeType) {
        return new NormalPrice();
    }

    public double getFirstPrice() {
        return firstPrice;
    }

    public void setFirstPrice(double firstPrice) {
        this.firstPrice = firstPrice;
    }

    public int getFirstDuration() {
        return firstDuration;
    }

    public void setFirstDuration(int firstDuration) {
        this.firstDuration = firstDuration;
    }

    public double getStepPrice() {
        return stepPrice;
    }

    public void setStepPrice(double stepPrice) {
        this.stepPrice = stepPrice;
    }

    public int getStepDuration() {
        return stepDuration;
    }

    public void setStepDuration(int stepDuration) {
        this.stepDuration = stepDuration;
    }


}
