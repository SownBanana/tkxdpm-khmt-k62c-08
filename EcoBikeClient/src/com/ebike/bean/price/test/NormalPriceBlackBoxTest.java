package com.ebike.bean.price.test;

import com.ebike.bean.bike.BikeType;
import com.ebike.bean.price.NormalPrice;
import com.ebike.bean.price.Price;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class NormalPriceBlackBoxTest {
    private Date startDate;
    private Date finishDate;
    private double expectedResult;

    public NormalPriceBlackBoxTest(Date startDate, Date finishDate, double expectedResult) {
        super();
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> primeNumbers() {
        return Arrays.asList(new Object[][]{
                {new Date(1999, 12, 24, 9, 0), new Date(1999, 12, 24, 8, 30), 0},
                {new Date(1999, 12, 24, 9, 0), new Date(1999, 12, 24, 9, 05), 0},
                {new Date(1999, 12, 24, 9, 0), new Date(1999, 12, 24, 9, 25), 10000},
                {new Date(1999, 12, 24, 9, 0), new Date(1999, 12, 24, 9, 45), 13000}
        });
    }

    @Test
    public void testCalculate() {
        //PriceManager priceManager=PriceManager.getInstance();
        //Price price=priceManager.getPrice(PriceType.NORMAL, BikeType.EBIKE);
        Price price = new NormalPrice(500000, BikeType.EBIKE, 10, 10000, 30, 3000, 15);
        assertEquals((int) expectedResult, (int) price.calculatePrice(startDate, finishDate));
    }
}
