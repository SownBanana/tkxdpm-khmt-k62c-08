package com.ebike.bean.price.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ NormalPriceBlackBoxTest.class, NormalPriceWhiteBoxTest.class})
public class NormalPriceTestSuite {

}