package com.ebike.bean.price;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Date;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = NormalPrice.class, name = "normalPrice"),
        @JsonSubTypes.Type(value = Hours24Price.class, name = "hourPrice"),
})

public abstract class Price {
    private double deposit;
    private String bikeType;
    private int freeFeeTime;

    public Price(double deposit, String bikeType, int freeFeeTime) {
        this.deposit = deposit;
        this.bikeType = bikeType;
        this.freeFeeTime = freeFeeTime;
    }

    public Price() {
    }

    public abstract double calculatePrice(Date startDate, Date finishDate);

    public abstract Price createPrice(String bikeType);

    public double getDeposit() {
        return deposit;
    }

    public void setDeposit(double deposit) {
        this.deposit = deposit;
    }

    public String getBikeType() {
        return bikeType;
    }

    public void setBikeType(String bikeType) {
        this.bikeType = bikeType;
    }

    public int getFreeFeeTime() {
        return freeFeeTime;
    }

    public void setFreeFeeTime(int freeFeeTime) {
        this.freeFeeTime = freeFeeTime;
    }
}
