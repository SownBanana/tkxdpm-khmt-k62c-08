package com.ebike.bean.price;

import java.util.Date;

public class Hours24Price extends Price{
	private double longRentPrice;
	private int longRentDuration;
	private int refundPivot;
	private double refundStepPrice;
	private int refundStepDuration;
	private double overtimeStepPrice;
	private int overtimeStepDuration;

    public Hours24Price(double deposit, String bikeType, int freeFeeTime, double longRentPrice, int longRentDuration, int refundPivot, double refundStepPrice, int refundStepDuration, double overtimeStepPrice, int overtimeStepDuration) {
        super(deposit, bikeType, freeFeeTime);
        this.longRentPrice = longRentPrice;
        this.longRentDuration = longRentDuration;
        this.refundPivot = refundPivot;
        this.refundStepPrice = refundStepPrice;
        this.refundStepDuration = refundStepDuration;
        this.overtimeStepPrice = overtimeStepPrice;
        this.overtimeStepDuration = overtimeStepDuration;
    }

    public Hours24Price(){
        super();
    }

    @Override
    public Price createPrice(String bikeType) {
        return new Hours24Price();
    }


    @Override
	public double calculatePrice(Date startDate, Date finishDate){
		long minutes=(finishDate.getTime()-startDate.getTime())/(60*60);
		double rs=0;
		if(minutes<0)
		    return 0;
		if(minutes> getFreeFeeTime()){
            if(minutes > (refundPivot - refundStepDuration) && minutes <= longRentDuration)
                rs = longRentPrice;
            else if(minutes <= (refundPivot - refundStepDuration)) {
                rs = longRentPrice - refundStepPrice*(refundPivot - minutes)/refundStepDuration;
            }else {
                int step=(int)(minutes-longRentDuration)/overtimeStepDuration;
                if(step*overtimeStepDuration<(minutes-longRentDuration))
                    step++;
                rs = longRentPrice + overtimeStepPrice*step;
            }
		}
		return rs;
	}

    public double getLongRentPrice() {
        return longRentPrice;
    }

    public void setLongRentPrice(double longRentPrice) {
        this.longRentPrice = longRentPrice;
    }

    public int getLongRentDuration() {
        return longRentDuration;
    }

    public void setLongRentDuration(int longRentDuration) {
        this.longRentDuration = longRentDuration;
    }

    public int getRefundPivot() {
        return refundPivot;
    }

    public void setRefundPivot(int refundPivot) {
        this.refundPivot = refundPivot;
    }

    public double getRefundStepPrice() {
        return refundStepPrice;
    }

    public void setRefundStepPrice(double refundStepPrice) {
        this.refundStepPrice = refundStepPrice;
    }

    public int getRefundStepDuration() {
        return refundStepDuration;
    }

    public void setRefundStepDuration(int refundStepDuration) {
        this.refundStepDuration = refundStepDuration;
    }

    public double getOvertimeStepPrice() {
        return overtimeStepPrice;
    }

    public void setOvertimeStepPrice(double overtimeStepPrice) {
        this.overtimeStepPrice = overtimeStepPrice;
    }

    public int getOvertimeStepDuration() {
        return overtimeStepDuration;
    }

    public void setOvertimeStepDuration(int overtimeStepDuration) {
        this.overtimeStepDuration = overtimeStepDuration;
    }


}
