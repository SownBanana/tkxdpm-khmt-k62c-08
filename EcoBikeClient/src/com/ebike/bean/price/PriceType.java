package com.ebike.bean.price;

import java.util.ArrayList;
import java.util.List;

public class PriceType {
    public static final String HOUR="hourPrice";
    public static final String NORMAL="normalPrice";
    public static ArrayList<String> getAllPriceType(){
        ArrayList<String> priceTypies=new ArrayList<>();
        priceTypies.add(HOUR);
        priceTypies.add(NORMAL);
        return priceTypies;
    }
}
