package com.ebike.bean.price;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ebike.serverapi.price.HourPriceApi;
import com.ebike.serverapi.price.IPriceApi;
import com.ebike.serverapi.price.NormalPriceApi;

public class PriceManager {

    private IPriceApi iPriceApi;
    private static HashMap<String,Price> priceHashMap;

    private static PriceManager priceManager=new PriceManager();
    public static PriceManager getInstance(){
        return priceManager;
    }
    private PriceManager(){
        priceHashMap=new HashMap<>();
    }

    // price don't change regularly, so should be load ( get data from server) when
    // user start application
    public Price getPrice(String priceType,String bikeType){
        Price price=priceHashMap.get(priceType+bikeType);
        if(price!=null)
            return price;
        if(priceType.equals(PriceType.HOUR)){ 
            iPriceApi=new HourPriceApi();
        }
        else if(priceType.equals(PriceType.NORMAL)){
            iPriceApi=new NormalPriceApi();
        }
        try {
            Price priceResult= (Price) iPriceApi.getData(bikeType);
            priceHashMap.put(priceType+bikeType,priceResult);
            return priceResult;
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public List<Price> getAllPriceByBikeType(String bikeType){
        ArrayList<Price> prices=new ArrayList<>();
        List<String> priceList= PriceType.getAllPriceType();
        for (String priceType:priceList
             ) {
            prices.add(getPrice(priceType,bikeType));
        }
        return prices;
    }
}
