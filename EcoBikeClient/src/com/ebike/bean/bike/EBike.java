package com.ebike.bean.bike;

public class EBike extends Bike{

	public EBike() {
		super();
		// TODO Auto-generated constructor stub
	}


	public EBike(String id, String name, String description, String brand, String licence, int booking) {
		super(id, name, description, brand, licence, booking);
	}

	public EBike(String id, String name, String description, String brand, String licence) {
		super(id, name, description, brand, licence);
	}


	@Override
	public boolean match(Bike bike) {
		// TODO Auto-generated method stub
		if(!(bike instanceof EBike)) return false;
		return super.match(bike);
		
	}
}
