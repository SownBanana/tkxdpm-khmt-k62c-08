package com.ebike.bean.bike;

public class TwinBike extends Bike{
	
	public TwinBike() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TwinBike(String id, String name, String description, String brand, String licence, int booking) {
		super(id, name, description, brand, licence, booking);
	}

	public TwinBike(String id, String name, String description, String brand, String licence) {
		super(id, name, description, brand, licence);
	}

	@Override
	public boolean match(Bike bike) {
		// TODO Auto-generated method stub
		if(!(bike instanceof TwinBike)) return false;
		return super.match(bike);
		
	}
}
