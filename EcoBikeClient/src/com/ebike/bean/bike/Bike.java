package com.ebike.bean.bike;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "class")
@JsonSubTypes({ 
	@JsonSubTypes.Type(value = RegularBike.class, name = "regularBike"), 
	@JsonSubTypes.Type(value = TwinBike.class, name = "twinBike"), 
	@JsonSubTypes.Type(value = EBike.class, name = "eBike")
})

public class Bike {
	private String id;
	private String name;
	private String description;
	private String brand;
	private String licence;
	private int booking;
	
	public Bike() {
		super();
	}
	
	public Bike(String id, String name, String description, String brand, String licence) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.brand = brand;
		this.licence = licence;
		this.booking = 0;
	}

	public Bike(String id, String name, String description, String brand, String licence, int booking
			) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.brand = brand;
		this.licence = licence;
		this.booking = booking;
	}

	public boolean matchOnce(Bike bike) {
		if (bike == null)
			return false;
		if (bike.id != null && (bike.id.equals("") || this.id.toLowerCase().equals(bike.id.toLowerCase()))) {
			return true;
		}

		if (bike.name != null && (bike.name.equals("") || this.name.toLowerCase().contains(bike.name.toLowerCase()))) {
			return true;
		}
		if (bike.description != null && (bike.description.equals("") || this.description.toLowerCase().contains(bike.description.toLowerCase()))) {
			return true;
		}
		if (bike.brand != null && (bike.brand.equals("") || this.brand.toLowerCase().contains(bike.brand.toLowerCase()))) {
			return true;
		}
		if (bike.licence != null && (bike.licence.equals("") || this.licence.toLowerCase().contains(bike.licence.toLowerCase()))) {
			return true;
		}
		return false;
	}
	
	public boolean match(Bike bike) {
		if (bike == null)
			return true;
		if (bike.id != null && !bike.id.equals("") && !this.id.toLowerCase().equals(bike.id.toLowerCase())) {
			return false;
		}

		if (bike.name != null && !bike.name.equals("") && !this.name.toLowerCase().contains(bike.name.toLowerCase())) {
			return false;
		}
		if (bike.description != null && !bike.description.equals("") && !this.description.toLowerCase().contains(bike.description.toLowerCase())) {
			return false;
		}
		if (bike.brand != null && !bike.brand.equals("") && !this.brand.toLowerCase().contains(bike.brand.toLowerCase())) {
			return false;
		}
		if (bike.licence != null && !bike.licence.equals("") && !this.licence.toLowerCase().contains(bike.licence.toLowerCase())) {
			return false;
		}
		return true;
	}

	public boolean matchAsBike(Bike bike) {
		return this.match(bike);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Bike) {
			return this.id.equals(((Bike) obj).id);
		}
		return false;
	}

	@Override
	public String toString() {
		return "Bike [id=" + id + ", name=" + name + ", description=" + description + ", brand=" + brand + ", licence="
				+ licence + ", booking=" + booking 
//				+ ", price=" + price 
				+ "]";
	}

	public String genBikeType(){
		String className= getClass().getSimpleName();
		return className.substring(0,1).toLowerCase()+className.substring(1,className.length());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getLicence() {
		return licence;
	}

	public void setLicence(String licence) {
		this.licence = licence;
	}

	public int getBooking() {
		return booking;
	}

	public void setBooking(int booking) {
		this.booking = booking;
	}
	
}
