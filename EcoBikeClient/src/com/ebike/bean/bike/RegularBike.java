package com.ebike.bean.bike;

public class RegularBike extends Bike{
	
	public RegularBike() {
		super();
		// TODO Auto-generated constructor stub
	}


	public RegularBike(String id, String name, String description, String brand, String licence, int booking) {
		super(id, name, description, brand, licence, booking);
		// TODO Auto-generated constructor stub
	}

	public RegularBike(String id, String name, String description, String brand, String licence) {
		super(id, name, description, brand, licence);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public boolean match(Bike bike) {
		// TODO Auto-generated method stub
		if(!(bike instanceof RegularBike)) return false;
		return super.match(bike);
		
	}
}
