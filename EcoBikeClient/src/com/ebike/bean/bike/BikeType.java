package com.ebike.bean.bike;

public class BikeType {
    public static final String EBIKE="eBike";
    public static final String REGULARBIKE="regularBike";
    public static final String TWINBIKE="twinBike";
}
