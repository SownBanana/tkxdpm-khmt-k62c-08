package com.ebike.bean.rental;

import com.ebike.bean.bike.Bike;
import com.ebike.bean.price.Price;
import com.ebike.bean.price.PriceManager;

import java.util.Date;
public class RentalDetail {
	
	private Date startDate;
	
	private Date finishDate;

	private String username;

	private Bike bike;

	private String rentalType;

	private double rentalPrice;

	public RentalDetail() {
		startDate=new Date();
	}


	public RentalDetail(Date startDate, Date finishDate, String username, Bike bike, String rentalType) {
		this.startDate = startDate;
		this.finishDate = finishDate;
		this.username = username;
		this.bike = bike;
		this.rentalType = rentalType;
	}

	public String getTypeBike(){
		if(bike!=null)
			return bike.genBikeType();
		return null;
	}
	public Price getPrice(){
		return PriceManager.getInstance().getPrice(rentalType,bike.genBikeType());
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Bike getBike() {
		return bike;
	}

	public void setBike(Bike bike) {
		this.bike = bike;
	}

	public String getRentalType() {
		return rentalType;
	}

	public void setRentalType(String rentalType) {
		this.rentalType = rentalType;
	}
}
