package com.ebike.main;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.ebike.main.controller.EcoAdminController;
import com.ebike.main.controller.EcoUserController;

public class EcoAdmin  extends JFrame{
	public static final int WINDOW_WIDTH = 800;
    public static final int WINDOW_HEIGHT = 550;
    
	public EcoAdmin(EcoAdminController controller) {
		JPanel rootPanel = new JPanel();
        setContentPane(rootPanel);
        BorderLayout layout = new BorderLayout();
        rootPanel.setLayout(layout);
        setTitle("EcoBike System for Admin");
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setLocationRelativeTo(null);
        setVisible(true);

        JTabbedPane tabbedPane = new JTabbedPane();
        rootPanel.add(controller.getStationListPane(), BorderLayout.CENTER);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	 public static void main(String[] args) {
	        try {
	            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	        } catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        } catch (InstantiationException e) {
	            e.printStackTrace();
	        } catch (IllegalAccessException e) {
	            e.printStackTrace();
	        } catch (UnsupportedLookAndFeelException e) {
	            e.printStackTrace();
	        }

	        SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
	                new EcoAdmin(new EcoAdminController());
	            }
	        });
	    }
	
}
