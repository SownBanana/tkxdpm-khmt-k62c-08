package com.ebike.main.controller;


import com.ebike.screen.rental.controller.RentalController;
import javax.swing.*;

import com.ebike.screen.station.controller.StationController;

public class EcoUserController {
    private RentalController rentalController;
    private StationController stationController;
    public EcoUserController(){

//       rentalController=new RentalController();
        stationController=new StationController();
      
      

        rentalController=RentalController.getInstace();

    }

    public JPanel getRentalPane(){
    	 
        return rentalController.getRentalPane();
        
    }
    
  
    public JPanel getStationListPane(){
        return stationController.getStationListPane();
    }
}
