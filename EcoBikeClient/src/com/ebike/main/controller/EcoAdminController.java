package com.ebike.main.controller;

import javax.swing.JPanel;

import com.ebike.screen.station.controller.StationController;

import com.ebike.screen.rental.controller.RentalController;
public class EcoAdminController {

	 private StationController stationController;
	    public EcoAdminController(){
	       
	        stationController=new StationController();
	      
	    }

	    public JPanel getStationListPane(){
	        return stationController.getAdminStationListPane();
	    }
}
