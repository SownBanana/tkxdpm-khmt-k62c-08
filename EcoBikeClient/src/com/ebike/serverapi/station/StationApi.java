package com.ebike.serverapi.station;

import com.ebike.bean.bike.Bike;
import com.ebike.bean.bike.EBike;
import com.ebike.bean.station.Station;
import com.ebike.serverapi.base.BaseConfiguration;
import com.ebike.serverapi.price.NormalPriceApi;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StationApi {
    private static StationApi stationApi=new StationApi();

    public static StationApi getInstance(){
        return stationApi;
    }

    
    public ArrayList<Station> getAllStation() {
        WebTarget webTarget = BaseConfiguration.getWebTarget().path("stations");
       
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>() {});
        System.out.println(res);
        return res;
    }
    public ArrayList<Station> getStations(Map<String, String> queryParams) {
    	WebTarget webTarget = BaseConfiguration.getWebTarget().path("stations");

        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>() {});
        System.out.println(res);
        return res;
    }

    public ArrayList<Station> getStationHaveFreeDock(){
        WebTarget webTarget = BaseConfiguration.getWebTarget().path("stations");
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<Station> resultDb = response.readEntity(new GenericType<ArrayList<Station>>() {});
        ArrayList<Station> result=new ArrayList<>();
        for (Station station: resultDb
             ) {
            if(station.getNumberOfEmptyDocks()>0)
                result.add(station);
        }
        return result;
    }



    public Station updateStation(Station station) {
    	WebTarget webTarget = BaseConfiguration.getWebTarget().path("stations").path("update");
//    	station.setBikes(null);
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(station, MediaType.APPLICATION_JSON));

        Station res = response.readEntity(Station.class);
        return res;
    }
    public Station addStation(Station station) {
    	WebTarget webTarget = BaseConfiguration.getWebTarget().path("stations").path("add");

        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(station, MediaType.APPLICATION_JSON));

        Station res = response.readEntity(Station.class);
        return res;
    }

    public Station deleteStation(Station station) {
    	WebTarget webTarget = BaseConfiguration.getWebTarget().path("stations").path("delete");
    	
    	webTarget = webTarget.queryParam("id", station.getId());
    	
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        Station res = response.readEntity(new GenericType<Station>() {});
        return res;
    }
    
    
    public Bike changeStation(String bikeId, String stationId) {
    	WebTarget webTarget = BaseConfiguration.getWebTarget().path("stations").path("change-station");
    	webTarget = webTarget.queryParam("bikeid", bikeId);
    	webTarget = webTarget.queryParam("des-stationid", stationId);
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        System.out.println(response);
        Bike res = null;
        try {
            res = response.readEntity(new GenericType<Bike>() {
            });
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return res;
    }

    public Bike rentBike(String stationId, String bikeId) {
    	WebTarget webTarget = BaseConfiguration.getWebTarget().path("stations").path("rentbike");

    	webTarget = webTarget.queryParam("stationid", stationId);
    	webTarget = webTarget.queryParam("bikeid", bikeId);

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        try {
        	Bike res = response.readEntity(new GenericType<Bike>() {});
        	System.out.println(res);			
        	return res;
		} catch (Exception e) {
			return null;
		}
    }
    public Bike returnBike(String stationId, String bikeId) {
    	WebTarget webTarget = BaseConfiguration.getWebTarget().path("stations").path("returnbike");

    	webTarget = webTarget.queryParam("stationid", stationId);
    	webTarget = webTarget.queryParam("bikeid", bikeId);

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        try {
        	Bike res = response.readEntity(new GenericType<Bike>() {});
        	System.out.println(res);			
        	return res;
		} catch (Exception e) {
			return null;
		}
    }
}
