package com.ebike.serverapi.station.test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.ebike.bean.bike.Bike;
import com.ebike.bean.station.Station;
import com.ebike.serverapi.bike.BikeApi;
import com.ebike.serverapi.station.StationApi;

class StationApiTest {
	StationApi api = StationApi.getInstance();
	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testGetStation() {
		ArrayList<Station> list= api.getStations(null);
		assertEquals("Error in getBooks API!", list.size(), 3);
	}
	@Test
	void testUpdateStation() {
		Station station = StationApi.getInstance().getStations(new HashMap<String, String>()).get(0);
		String stationNameString = station.getName();
		station.setName(stationNameString + " test");
		api.updateStation(station);
		Station rsstStation = StationApi.getInstance().getStations(new HashMap<String, String>(Collections.singletonMap("name", stationNameString + " test"))).get(0);
		assertEquals("Error in getBooks API!", station.getName(), rsstStation.getName());
		station.setName(stationNameString);
		api.updateStation(station);
	}
	@Test
	void testRentBike() {
		ArrayList<Bike> bikes = BikeApi.getInstance().getBikes("2", null);
		int bikesNumber = bikes.size();
		String bikeId = bikes.get(0).getId();
		api.rentBike("2", bikeId);
		ArrayList<Bike> afterBikes = BikeApi.getInstance().getBikes("2", null);
		assertEquals("Error in rentBike API!", afterBikes.size(), bikesNumber - 1);
		api.returnBike("2", bikeId);
	}
	@Test
	void testReturn() {
		ArrayList<Bike> bikes = BikeApi.getInstance().getBikes("2", null);
		int bikesNumber = bikes.size();
		String bikeId = bikes.get(0).getId();
		api.rentBike("2", bikeId);
		api.returnBike("2", bikeId);
		ArrayList<Bike> afterBikes = BikeApi.getInstance().getBikes("2", null);
		assertEquals("Error in returnBike API!", afterBikes.size(), bikesNumber);
	}
}
