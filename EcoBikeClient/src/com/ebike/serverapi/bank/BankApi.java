package com.ebike.serverapi.bank;

import com.ebike.bean.bank.BankAccount;
import com.ebike.bean.bank.BankTranfer;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class BankApi implements IBankApi {

    private static BankApi bankApi = new BankApi();
    public static final String PATH = "http://localhost:3000/";
    
    public static final int SUCCESS=1;
    public static final int FAIL=-1;
    public static final int WRONG_INFOR=2;
    public static final int BALACE_NOT_ENOUGH=3;
    private Client client;

    private BankApi() {
        client = ClientBuilder.newClient();
    }

    public static BankApi getInstance() {
        return bankApi;
    }

    @Override
    public boolean transferUserToSystem(BankAccount sender, double payment) {

        BankTranfer tranfer = new BankTranfer();
        tranfer.setAccA(sender);
        tranfer.setMoneyTransfer(payment);
        WebTarget webTarget = client.target(PATH).path("transferAtoB");
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(tranfer, MediaType.APPLICATION_JSON));
        if ("ok".equals(response.readEntity(String.class)))
            return true;
        return false;
    }

    @Override
    public boolean transferSystemToUser(BankAccount sender, double payment) {
        BankTranfer tranfer = new BankTranfer();
        tranfer.setAccA(sender);
        tranfer.setMoneyTransfer(payment);
        WebTarget webTarget = client.target(PATH).path("transferBtoA");
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(tranfer, MediaType.APPLICATION_JSON));
        if ("ok".equals(response.readEntity(String.class)))
            return true;
        return false;
    }

    @Override
    public double getBalance(BankAccount bankAccount) {
        return 0;
    }
    public int tranferReturn(BankAccount sender, double payment) {
    	 BankTranfer tranfer = new BankTranfer();
         tranfer.setAccA(sender);
         tranfer.setMoneyTransfer(payment);
         WebTarget webTarget = client.target(PATH).path("apitest"); //account balace not change after request
         Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
         Response response = invocationBuilder.post(Entity.entity(tranfer, MediaType.APPLICATION_JSON));
         String rs=response.readEntity(String.class);
         if(rs.equals("ok")) return SUCCESS;
         if(rs.equals("Account not right")) return WRONG_INFOR;
         if(rs.equals("transacion fail")) return BALACE_NOT_ENOUGH;
         return FAIL;
    }
    
    public static void main(String[] args) {

    }
}
