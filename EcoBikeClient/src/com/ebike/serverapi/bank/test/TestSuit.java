package com.ebike.serverapi.bank.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ BlackBoxBankApi.class, WhiteBoxBankApi.class})
public class TestSuit {

}