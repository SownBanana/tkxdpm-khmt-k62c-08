package com.ebike.serverapi.bank.test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.ebike.bean.bank.BankAccount;
import com.ebike.serverapi.bank.BankApi;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

@RunWith(Parameterized.class)
public class BlackBoxBankApi {
	private int expected;
    private String cardHolderName;
    private String cardNumber;
    private String issuingBank;
    private String securityCode;
    private String expirationDate;
    private double moneyTransfer;
    
    public BlackBoxBankApi(int expected,String cardHolderName, String cardNumber, String issuingBank, String securityCode, String expirationDate,double payment) {
		super();
		this.expected=expected;
		this.cardHolderName = cardHolderName;
		this.cardNumber = cardNumber;
		this.issuingBank = issuingBank;
		this.securityCode = securityCode;
		this.expirationDate = expirationDate;
		this.moneyTransfer=payment;
	}
    @Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][] { 
			{BankApi.SUCCESS, "le ngoc dong","19024705183013","bidv","100","12/24",1000 },
			{BankApi.WRONG_INFOR, "le ngoc dong","19024705183013","bidv","190","12/24",1000 },
			{BankApi.BALACE_NOT_ENOUGH, "le ngoc dong","19024705183013","bidv","100","12/24",9000000000.0},
		});
		
	}
	@Test
	public void testBank() {	
		assertEquals( expected,BankApi.getInstance().tranferReturn(new BankAccount(cardHolderName,cardNumber,issuingBank,securityCode,expirationDate),moneyTransfer));
	}
	
}
