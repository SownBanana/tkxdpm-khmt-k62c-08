package com.ebike.serverapi.bank;

import com.ebike.bean.bank.BankAccount;

public interface IBankApi {
    //  tru tien, cong tien, xem so du
    public boolean transferUserToSystem(BankAccount sender, double payment);
    public boolean transferSystemToUser(BankAccount sender, double payment);
    public double getBalance(BankAccount bankAccount);
}
