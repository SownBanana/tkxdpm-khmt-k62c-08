package com.ebike.serverapi.bank;

import com.ebike.bean.bank.BankAccount;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public class BaseConfiguration {
    private static final String PATH = "http://localhost:8081/";
    private static Client client= ClientBuilder.newClient();
    public static WebTarget getWebTarget(){
        return client.target(PATH);
    }
    public static BankAccount BANKSYSTEMACCOUNT=new BankAccount();
}
