package com.ebike.serverapi.price;

import com.ebike.bean.price.NormalPrice;
import com.ebike.serverapi.base.BaseConfiguration;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

public class NormalPriceApi implements IPriceApi<NormalPrice> {

    private static NormalPriceApi normalPriceApi=new NormalPriceApi();

    public NormalPriceApi getInstance(){
        return normalPriceApi;
    }

    @Override
    public ArrayList<NormalPrice> getDatas() {
        WebTarget webTarget = BaseConfiguration.getWebTarget().path("price/normal-price");
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<NormalPrice> res = response.readEntity(new GenericType<ArrayList<NormalPrice>>() {});
        System.out.println(res);
        return res;
    }

    @Override
    public NormalPrice getData(String typeBike) {
        WebTarget webTarget = BaseConfiguration.getWebTarget().path("price/normal-price");
        if (typeBike != null) {
            webTarget= webTarget.queryParam("bike-type",typeBike);
        }
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        NormalPrice res = response.readEntity(new GenericType<NormalPrice>() {});
        System.out.println(res);
        return res;
    }
}
