package com.ebike.serverapi.price;

import com.ebike.bean.price.Hours24Price;

import java.util.ArrayList;

public class TestAPI {
    private static IPriceApi iPriceApi;
    public static void main(String[] args) {
        iPriceApi=new HourPriceApi();
        Hours24Price res=(Hours24Price) iPriceApi.getData("eBike");
        System.out.println(res.getLongRentPrice());
    }
}
