package com.ebike.serverapi.price;

import com.ebike.bean.price.Hours24Price;
import com.ebike.serverapi.base.BaseConfiguration;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

public class HourPriceApi implements IPriceApi<Hours24Price> {

    private static HourPriceApi hours24PriceApi=new HourPriceApi();

    public HourPriceApi getInstance(){
        return hours24PriceApi;
    }

    @Override
    public ArrayList<Hours24Price> getDatas() {
        WebTarget webTarget = BaseConfiguration.getWebTarget().path("price/hour-prices");
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<Hours24Price> res = response.readEntity(new GenericType<ArrayList<Hours24Price>>() {});
        System.out.println(res);
        return res;
    }

    @Override
    public Hours24Price getData(String typeBike) {
        WebTarget webTarget = BaseConfiguration.getWebTarget().path("price/hour-price");
        if (typeBike != null) {
            webTarget= webTarget.queryParam("bike-type",typeBike);
        }
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        Hours24Price res = response.readEntity(new GenericType<Hours24Price>() {});
        System.out.println(res);
        return res;
    }
}
