package com.ebike.serverapi.price;

import javax.ws.rs.client.WebTarget;

import com.ebike.serverapi.base.BaseConfiguration;

import java.util.ArrayList;
import java.util.Map;

public interface IPriceApi<T> {
    WebTarget webTarget= BaseConfiguration.getWebTarget();
    ArrayList<T> getDatas();
    T getData(String typeBike);
}
