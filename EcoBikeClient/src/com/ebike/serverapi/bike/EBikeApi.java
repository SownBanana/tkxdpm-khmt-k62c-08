package com.ebike.serverapi.bike;

import com.ebike.bean.bike.Bike;
import com.ebike.bean.bike.EBike;
import com.ebike.serverapi.base.BaseConfiguration;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Map;

public class EBikeApi implements IBikeAPI<EBike> {
    private static EBikeApi eBikeApi=new EBikeApi();

    public static EBikeApi getInstance(){
        return eBikeApi;
    }

    @Override
    public ArrayList<EBike> getDatas(Map<String, String> queryParams, String stationId) {
        WebTarget webTarget = BaseConfiguration.getWebTarget().path("e-bike");
        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<EBike> res = response.readEntity(new GenericType<ArrayList<EBike>>() {});
        System.out.println(res);
        return res;
    }

    @Override
    public EBike update(EBike data, String stationId) {
        return null;
    }

    @Override
    public boolean delete(EBike data, String stationId) {
        return false;
    }

    @Override
    public EBike create(EBike data, String stationId) {

        return null;
    }

}
