package com.ebike.serverapi.bike;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebike.bean.bike.Bike;
import com.ebike.serverapi.base.BaseConfiguration;
import com.ebike.serverapi.station.StationApi;

public class BikeApi {
	private static BikeApi bikeApi=new BikeApi();

    public static BikeApi getInstance(){
        return bikeApi;
    }


    public ArrayList<Bike> getBikes(String stationId, Map<String, String> queryParams) {
    	 WebTarget webTarget = BaseConfiguration.getWebTarget().path("bikes");

        webTarget = webTarget.queryParam("stationid", stationId);
        
        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<Bike> res = response.readEntity(new GenericType<ArrayList<Bike>>() {});
        System.out.println(res);
        return res;
    }

    public Bike getBike(String bikeId) {
        return getBike(null, bikeId);
    }
    
    public Bike getBike(String stationId, String bikeId) {
    	 WebTarget webTarget = BaseConfiguration.getWebTarget().path("bikes");
        if(stationId != null)
        	webTarget = webTarget.queryParam("stationid", stationId);
        webTarget = webTarget.queryParam("id", bikeId);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<Bike> res = response.readEntity(new GenericType<ArrayList<Bike>>() {});
        Bike book = res.get(res.size()-1);
        System.out.println(book);
        return book;
    }


    public Bike updateBike(Bike book) {
    	 WebTarget webTarget = BaseConfiguration.getWebTarget().path(book.getId());

        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(book, MediaType.APPLICATION_JSON));

        Bike res = response.readEntity(Bike.class);
        return res;
    }


}
