package com.ebike.serverapi.bike;

public class BikeApiFactory {
    private static BikeApiFactory bikeManager=new BikeApiFactory();

    private IBikeAPI iBikeAPI;
    public static BikeApiFactory getInstance(){
        return bikeManager;
    }

    public IBikeAPI getBikeApi(String bikeType){
        bikeType=bikeType.toLowerCase();
        if(bikeType.equals("EBIKE"))
            iBikeAPI=new EBikeApi();
        else if(bikeType.equals("REGULARBIKE"))
            iBikeAPI=new RegularBikeApi();
        else if(bikeType.equals("TWINBIKE"))
            iBikeAPI=new TwinBikeAPI();
        return iBikeAPI;
    }
}
