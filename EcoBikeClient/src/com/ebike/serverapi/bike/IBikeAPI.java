package com.ebike.serverapi.bike;

import javax.ws.rs.client.WebTarget;

import com.ebike.serverapi.base.BaseConfiguration;

import java.util.ArrayList;
import java.util.Map;

public interface IBikeAPI<T>{
    WebTarget webtarget= BaseConfiguration.getWebTarget();

    ArrayList<T> getDatas(Map<String, String> queryParams, String stationId);
    T update(T data, String stationId);
    boolean delete(T data, String stationId);
    T create(T data, String stationId);
}
