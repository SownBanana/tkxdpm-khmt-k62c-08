package com.ebike.serverapi.bike;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebike.serverapi.base.BaseConfiguration;

import java.util.ArrayList;
import java.util.Map;

public class TwinBikeAPI implements IBikeAPI<TwinBikeAPI> {

    private static TwinBikeAPI twinBikeAPI=new TwinBikeAPI();

    public static TwinBikeAPI getInstance(){
        return twinBikeAPI;
    }

    @Override
    public ArrayList<TwinBikeAPI> getDatas(Map<String, String> queryParams, String stationId) {
        WebTarget webTarget = BaseConfiguration.getWebTarget().path("regular-bike");
        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<TwinBikeAPI> res = response.readEntity(new GenericType<ArrayList<TwinBikeAPI>>() {});
        System.out.println(res);
        return res;
    }
    @Override
    public TwinBikeAPI update(TwinBikeAPI data, String stationId) {
        return null;
    }

    @Override
    public boolean delete(TwinBikeAPI data, String stationId) {
        return false;
    }

    @Override
    public TwinBikeAPI create(TwinBikeAPI data, String stationId) {
        return null;
    }
}
