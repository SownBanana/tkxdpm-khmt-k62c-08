package com.ebike.serverapi.bike;

import com.ebike.bean.bike.EBike;
import com.ebike.bean.bike.RegularBike;
import com.ebike.serverapi.base.BaseConfiguration;
import com.ebike.serverapi.bike.IBikeAPI;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Map;

public class RegularBikeApi implements IBikeAPI<RegularBike> {

    private static RegularBikeApi regularBikeApi=new RegularBikeApi();

    public static RegularBikeApi getInstance(){
        return regularBikeApi;
    }

    @Override
    public ArrayList<RegularBike> getDatas(Map<String, String> queryParams, String stationId) {
        WebTarget webTarget = BaseConfiguration.getWebTarget().path("regular-bike");
        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<RegularBike> res = response.readEntity(new GenericType<ArrayList<RegularBike>>() {});
        System.out.println(res);
        return res;
    }

    @Override
    public RegularBike update(RegularBike data, String stationId) {
        return null;
    }

    @Override
    public boolean delete(RegularBike data, String stationId) {
        return false;
    }

    @Override
    public RegularBike create(RegularBike data, String stationId) {
        return null;
    }
}
