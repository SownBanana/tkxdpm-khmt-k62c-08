package com.ebike.serverapi.bike.test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.ebike.bean.bike.Bike;
import com.ebike.serverapi.bike.BikeApi;


class BikeApiTest {
	BikeApi api = BikeApi.getInstance();
	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	public void testGetAllBikes() {
		ArrayList<Bike> list= api.getBikes(null, null);
		assertEquals("Error in getBooks API!", list.size(), 12);
	}
	

}
