package com.ebike.serverapi.base;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

// just only create Client to connect with Server
public class BaseConfiguration {
    private static final String PATH = "http://localhost:8080/";
    private static Client client= ClientBuilder.newClient();
    public static WebTarget getWebTarget(){
        return client.target(PATH);
    }
}
