package com.ebike.screen.dialog;

import com.ebike.screen.rental.controller.RentalController;
import com.ebike.bean.bank.BankAccount;
import com.ebike.screen.dialog.ShowMessage;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class BankInfoDialog extends JDialog {

    private GridBagLayout layout;
    private GridBagConstraints c;

    private JTextField cardHolderName;
    private JTextField cardNumber;
    private JTextField bankName;
    private JTextField expiredDate;
    private JTextField password;

    private RentalController controller;

    private static BankInfoDialog bankInfoDialog=new BankInfoDialog();
    public static BankInfoDialog getInstance(){
        return bankInfoDialog;
    }

    private BankInfoDialog(){
        layout=new GridBagLayout();
        this.setLayout(layout);
        c=new GridBagConstraints();
        //transfer(0);
    }

    public void setController(RentalController rentalController){
        this.controller=rentalController;
    }

    // payment >0: user send money to system
    // payment <0: system send money to user
    public void transfer(double payment){
        String message;
        if(payment>=0)
            message="Nhap thong tin va xac nhan chuyen tien";
        else {
            message = "Nhap thong tin, he thong se chuyen tien cho ban";
        }
        this.getContentPane().removeAll();
        BankAccount bankAccount=new BankAccount();
        this.getContentPane().removeAll();
        c.gridx=0;
        c.gridy=0;
        add(new JLabel("CHUYEN KHOAN: "+message),c);
        c.insets= new Insets(10,10,5,10);
        c.gridx=0;
        c.gridy=1;
        add(new JLabel("Ten chu the"),c);
        c.gridx=1;
        c.gridy=1;
        cardHolderName=new JTextField(15);
        add(cardHolderName,c);

        c.insets=new Insets(0,0,5,0);
        c.gridx=0;
        c.gridy=2;
        add(new JLabel("So tai khoan"),c);
        c.gridx=1;
        c.gridy=2;
        cardNumber=new JTextField(15);
        add(cardNumber,c);

        c.gridx = 0;
        c.gridy = 3;
        add(new JLabel("Ngan hang"), c);
        c.gridx = 1;
        c.gridy = 3;
        bankName = new JTextField(15);
        add(bankName, c);

        c.gridx = 0;
        c.gridy = 4;
        add(new JLabel("Ma bao mat"), c);
        c.gridx = 1;
        c.gridy = 4;
        password = new JTextField(15);
        add(password, c);

        c.gridx = 0;
        c.gridy = 5;
        add(new JLabel("Ngay het han"), c);
        c.gridx = 1;
        c.gridy = 5;
        expiredDate = new JTextField(15);
        add(expiredDate, c);

        c.gridx = 0;
        c.gridy = 6;
        add(new JLabel("So tien"), c);
        c.gridx = 1;
        c.gridy = 6;
        add(new JLabel((payment>0?payment:-payment)+""), c);


        cardHolderName.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                bankAccount.setCardHolderName(cardHolderName.getText());
            }
        });

        cardNumber.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                bankAccount.setCardNumber(cardNumber.getText());
            }
        });

        bankName.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                bankAccount.setIssuingBank(bankName.getText());
            }
        });

        password.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                bankAccount.setSecurityCode(password.getText());
            }
        });

        expiredDate.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                bankAccount.setExpirationDate(expiredDate.getText());
            }
        });

        c.gridx=0;
        c.gridy=7;
        JButton button=new JButton("Thu hien giao dich");
        add(button,c);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.sendPayment(bankAccount,payment);
            }
        });

        this.revalidate();
        this.repaint();
        this.pack();
        this.setResizable(false);

        }



}
