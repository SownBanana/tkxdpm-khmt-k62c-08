package com.ebike.screen.dialog;

import javax.swing.*;
import java.awt.*;

public class ShowMessage {
    private static JFrame jFrame=new JFrame();

    public static void showInfoMessage(String message){
        System.out.println(jFrame);
        JOptionPane.showMessageDialog(jFrame,message, "Dialog",JOptionPane.INFORMATION_MESSAGE);
    }
    public static void showErrorMessage(String message){
        System.out.println(jFrame);
        JOptionPane.showMessageDialog(jFrame,message,"Dialog",JOptionPane.ERROR_MESSAGE);
    }
}
