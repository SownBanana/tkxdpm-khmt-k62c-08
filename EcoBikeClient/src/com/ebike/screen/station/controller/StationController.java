package com.ebike.screen.station.controller;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JFrame;

import com.ebike.bean.bike.Bike;
import com.ebike.bean.bike.EBike;
import com.ebike.bean.bike.RegularBike;
import com.ebike.bean.bike.TwinBike;
import com.ebike.bean.station.Station;
import com.ebike.screen.bike.gui.BikeInStationFrame;
import com.ebike.screen.station.gui.AddStationDialog;
import com.ebike.screen.station.gui.AdminStationListPanel;
import com.ebike.screen.station.gui.EditStationDialog;
import com.ebike.screen.station.gui.StationListPanel;
import com.ebike.screen.station.gui.StationSingleItem;
import com.ebike.serverapi.bike.BikeApi;
import com.ebike.serverapi.station.StationApi;
import com.ebike.screen.abstracts.controller.StationControllerAdapter;


public class StationController {
	private StationListPanel stationListPanel;
	private StationSingleItem singleItem;
	private AdminStationListPanel adminStationListPanel;
	private AddStationDialog addStationDialog;
	private EditStationDialog editStationDialog;
	private int numberRegularBike;
	private int numberEBike;
	private int numberTwinBike;
	
	public int getNumberRegularBike() {
		return numberRegularBike;
	}

	public void setNumberRegularBike(int numberRegularBike) {
		this.numberRegularBike = numberRegularBike;
	}

	public int getNumberEBike() {
		return numberEBike;
	}

	public void setNumberEBike(int numberEBike) {
		this.numberEBike = numberEBike;
	}

	public int getNumberTwinBike() {
		return numberTwinBike;
	}

	public void setNumberTwinBike(int numberTwinBike) {
		this.numberTwinBike = numberTwinBike;
	}

	public StationController() {
		stationListPanel=new StationListPanel(this);
		adminStationListPanel=new AdminStationListPanel(this);
		
		
		
	}
	
	public StationListPanel getStationListPane() {
		return stationListPanel;
	}
	
	
	public AdminStationListPanel getAdminStationListPane() {
		return adminStationListPanel;
	}
	
	
	public void viewBikes(Station station) {
		// thêm hàm gì đó
		BikeInStationFrame frame = new BikeInStationFrame(station);
		frame.setSize(700, 500);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
	}
	
	public void getNumberBike(ArrayList<Bike> bikes) {
		numberEBike=0;
		numberRegularBike=0;
		numberTwinBike=0;
		if(bikes != null)
			for (Bike bike : bikes) {
				if(bike instanceof RegularBike) numberRegularBike++;
				if(bike instanceof EBike) numberEBike++;
				if(bike instanceof TwinBike) numberTwinBike++;
			}
		

	}


	public void ShowAddStationDialog() {
		addStationDialog= new AddStationDialog();
		addStationDialog.setLocationRelativeTo(null);
		addStationDialog.setVisible(true);
		
	}

	public void ShowEditStationDialog(Station station) {
		// TODO Auto-generated method stub
		EditStationDialog dialog = new EditStationDialog(station, new StationControllerAdapter() {
			@Override
			public void update(Station t) {
				// TODO Auto-generated method stub
				System.out.println(StationApi.getInstance().updateStation(t));
			}
		});
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setVisible(true);
		dialog.setLocationRelativeTo(null);
		
	}

}
