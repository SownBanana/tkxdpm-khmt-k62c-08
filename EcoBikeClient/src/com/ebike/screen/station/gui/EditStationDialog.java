package com.ebike.screen.station.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.EmptyBorder;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import com.ebike.bean.bike.Bike;
import com.ebike.bean.station.Station;
import com.ebike.screen.abstracts.controller.IDataManageController;
import com.ebike.screen.abstracts.controller.StationControllerAdapter;
import com.ebike.screen.bike.gui.ManageBike;
import com.ebike.serverapi.bike.BikeApi;
import com.ebike.serverapi.station.StationApi;

import javassist.expr.NewArray;

import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import net.miginfocom.swing.MigLayout;
import javax.swing.BoxLayout;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JTextField;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;

import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

import javax.swing.JScrollPane;

public class EditStationDialog extends JDialog {

	private Station station;
	private IDataManageController<Station> controller;
	private final JPanel contentPanel = new JPanel();
	private JTextField nameField;
	private JTextField idField;
	private JTextArea addressArea;
	
	private JTable bikesTable;
	private DefaultTableModel bikesTableModel = new DefaultTableModel() {
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		};
	};
	
	String[] header = {"ID",
            "Tên xe",
            "Loại",
            "Biển số"};
	private Vector vctData = new Vector();
	
	

	private void fillBikesTable(ArrayList<Bike> bikes) {
		vctData.clear();
		for (Bike bike : bikes) {
			Vector rowVector = new Vector();
			rowVector.add(bike.getId());
			rowVector.add(bike.getName());
			rowVector.add(bike.getClass().getSimpleName());
			rowVector.add(bike.getLicence());
			vctData.add(rowVector);
		}
		bikesTableModel.setDataVector(vctData, new Vector(Arrays.asList(header)));
	}
	/**
	 * Create the dialog.
	 */
	public EditStationDialog(Station station, IDataManageController<Station> controller) {
		this.station = station;
		this.controller = controller;
		
		fillBikesTable(BikeApi.getInstance().getBikes(station.getId(), null));
		
		setBounds(100, 100, 700, 660);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[] {90, 300};
		gbl_contentPanel.rowHeights = new int[] {20, 20, 20, 20, 30, 30, 100};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel nameLabel = new JLabel(station.getName());
			nameLabel.setFont(new Font("Arial", Font.BOLD, 16));
			GridBagConstraints gbc_nameLabel = new GridBagConstraints();
			gbc_nameLabel.gridwidth = 2;
			gbc_nameLabel.insets = new Insets(0, 0, 5, 0);
			gbc_nameLabel.gridx = 0;
			gbc_nameLabel.gridy = 0;
			contentPanel.add(nameLabel, gbc_nameLabel);
		}
		{
			JLabel lblNewLabel_4 = new JLabel("ID:  ");
			GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
			gbc_lblNewLabel_4.anchor = GridBagConstraints.EAST;
			gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
			gbc_lblNewLabel_4.gridx = 0;
			gbc_lblNewLabel_4.gridy = 1;
			contentPanel.add(lblNewLabel_4, gbc_lblNewLabel_4);
		}
		{
			idField = new JTextField(station.getId());
			idField.setEditable(false);
			idField.setEnabled(false);
			GridBagConstraints gbc_idField = new GridBagConstraints();
			gbc_idField.insets = new Insets(0, 0, 5, 0);
			gbc_idField.fill = GridBagConstraints.HORIZONTAL;
			gbc_idField.gridx = 1;
			gbc_idField.gridy = 1;
			contentPanel.add(idField, gbc_idField);
			idField.setColumns(10);
		}
		{
			JLabel lblNewLabel_1 = new JLabel("T\u00EAn tr\u1EA1m xe:  ");
			GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
			gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
			gbc_lblNewLabel_1.fill = GridBagConstraints.VERTICAL;
			gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
			gbc_lblNewLabel_1.gridx = 0;
			gbc_lblNewLabel_1.gridy = 2;
			contentPanel.add(lblNewLabel_1, gbc_lblNewLabel_1);
		}
		{
			nameField = new JTextField(station.getName());
			GridBagConstraints gbc_nameField = new GridBagConstraints();
			gbc_nameField.insets = new Insets(0, 0, 5, 0);
			gbc_nameField.fill = GridBagConstraints.HORIZONTAL;
			gbc_nameField.gridx = 1;
			gbc_nameField.gridy = 2;
			contentPanel.add(nameField, gbc_nameField);
			nameField.setColumns(10);
		}
		{
			JLabel lblNewLabel = new JLabel("\u0110\u1ECBa ch\u1EC9:  ");
			GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
			gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
			gbc_lblNewLabel.fill = GridBagConstraints.VERTICAL;
			gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
			gbc_lblNewLabel.gridx = 0;
			gbc_lblNewLabel.gridy = 3;
			contentPanel.add(lblNewLabel, gbc_lblNewLabel);
		}
		{
			addressArea = new JTextArea(station.getAddress());
			GridBagConstraints gbc_addressArea = new GridBagConstraints();
			gbc_addressArea.gridheight = 3;
			gbc_addressArea.insets = new Insets(0, 0, 5, 0);
			gbc_addressArea.fill = GridBagConstraints.BOTH;
			gbc_addressArea.gridx = 1;
			gbc_addressArea.gridy = 3;
			contentPanel.add(addressArea, gbc_addressArea);
		}
		{

			bikesTable = new JTable();
			bikesTable.setAutoCreateRowSorter(true);
			bikesTable.setModel(bikesTableModel);
			final JPopupMenu popupMenu = new JPopupMenu();
	        JMenuItem editItem = new JMenuItem("Sửa");
	        editItem.addMouseListener(new MouseAdapter() {
	        	@Override
	        	public void mousePressed(MouseEvent e) {
	        		super.mousePressed(e);
	        		String bikeidString = (String) bikesTableModel.getValueAt(bikesTable.convertRowIndexToModel(bikesTable.getSelectedRow()), 0);
	        		System.out.println(bikeidString);
	        	}
			});
	        popupMenu.add(editItem);
	        JMenuItem addItem = new JMenuItem("Thêm xe");
	        addItem.addMouseListener(new MouseAdapter() {
	        	@Override
	        	public void mousePressed(MouseEvent e) {
	        		super.mousePressed(e);
	        		ManageBike frame = new ManageBike();
	        		frame.setLocationRelativeTo(null);
	        		frame.setVisible(true);
	        	}
			});
	        popupMenu.add(addItem);
	        JMenuItem relocateItem = new JMenuItem("Chuyển trạm");
	        relocateItem.addMouseListener(new MouseAdapter() {
	        	@Override
	        	public void mousePressed(MouseEvent e) {
	        		super.mousePressed(e);
	        		String bikeId = (String) bikesTableModel.getValueAt(bikesTable.convertRowIndexToModel(bikesTable.getSelectedRow()), 0);
	        		
	        		ChangeStationDialog dialog = new ChangeStationDialog(BikeApi.getInstance().getBike(station.getId(), bikeId), station);
	        		String dstStationId = dialog.showDialog();
	        		if(dstStationId != null) {
	        			Bike removedBike= StationApi.getInstance().changeStation(bikeId, dstStationId);
	        			if( removedBike != null){
	        				station.getBikes().remove(removedBike);
	        			}
	        			
		        		fillBikesTable(BikeApi.getInstance().getBikes(station.getId(), null));
	        		}
	        		
	        	}
			});
	        popupMenu.add(relocateItem);
	        popupMenu.addSeparator();
	        JMenuItem deleteItem = new JMenuItem("Xoá xe");
	        deleteItem.addMouseListener(new MouseAdapter() {
	        	@Override
	        	public void mousePressed(MouseEvent e) {
	        		super.mousePressed(e);
	        		String bikeidString = (String) bikesTableModel.getValueAt(bikesTable.convertRowIndexToModel(bikesTable.getSelectedRow()), 0);
	        		System.out.println(bikeidString);
	        	}
			});
	        popupMenu.add(deleteItem);
	        popupMenu.addPopupMenuListener(new PopupMenuListener() {

	            @Override
	            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
	                SwingUtilities.invokeLater(new Runnable() {
	                    @Override
	                    public void run() {
	                        int rowAtPoint = bikesTable.rowAtPoint(SwingUtilities.convertPoint(popupMenu, new Point(0, 0), bikesTable));
	                        if (rowAtPoint > -1) {
	                        	bikesTable.setRowSelectionInterval(rowAtPoint, rowAtPoint);
	                        }
	                    }
	                });
	            }

	            @Override
	            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
	                // TODO Auto-generated method stub

	            }

	            @Override
	            public void popupMenuCanceled(PopupMenuEvent e) {
	                // TODO Auto-generated method stub

	            }
	        });
	        
	        bikesTable.setComponentPopupMenu(popupMenu);
			JScrollPane scrollPane = new JScrollPane(bikesTable);
			bikesTable.setFillsViewportHeight(true);
			GridBagConstraints gbc_scrollPane = new GridBagConstraints();
			gbc_scrollPane.gridwidth = 2;
			gbc_scrollPane.fill = GridBagConstraints.BOTH;
			gbc_scrollPane.gridx = 0;
			gbc_scrollPane.gridy = 6;
			contentPanel.add(scrollPane, gbc_scrollPane);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton saveButton = new JButton("L\u01B0u");
				saveButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						boolean checkValid = true;
						String name = nameField.getText().trim();
						if(name.equals("")) {
							checkValid = false;
						}
						String address = addressArea.getText().trim();
						if(address.equals("")) {
							checkValid = false;
						}
						if(checkValid) {
							System.out.println("Update");
							station.setName(nameField.getText().trim());
							station.setAddress(addressArea.getText().trim());
							System.out.println(station);
							controller.update(station);
//							StationApi.getInstance().updateStation(station);
							dispose();
						}else {
							JOptionPane.showMessageDialog(null, "Không được để trống các trường");
						}
						
					}
				});
				saveButton.setActionCommand("Save");
				buttonPane.add(saveButton);
				getRootPane().setDefaultButton(saveButton);
			}
			{
				JButton cancelButton = new JButton("Tho\u00E1t");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				buttonPane.add(cancelButton);
			}
		}
	}

}
