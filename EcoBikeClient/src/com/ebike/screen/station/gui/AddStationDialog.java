package com.ebike.screen.station.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.GridBagLayout;

import com.ebike.bean.station.Station;
import com.ebike.serverapi.station.StationApi;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JEditorPane;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Color;

public class AddStationDialog extends JDialog {

	private final JPanel mainPanel = new JPanel();
	private JTextField nameField;
	JEditorPane addressEditor;
	private JLabel nameAlert;
	private JLabel addressAlert;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			AddStationDialog dialog = new AddStationDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public AddStationDialog() {
		setBounds(100, 100, 448, 199);
		getContentPane().setLayout(new BorderLayout());
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(mainPanel, BorderLayout.CENTER);
		GridBagLayout gbl_mainPanel = new GridBagLayout();
		gbl_mainPanel.columnWidths = new int[] {47, 300, 0, 0};
		gbl_mainPanel.rowHeights = new int[]{17, 20, 62, 0};
		gbl_mainPanel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_mainPanel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		mainPanel.setLayout(gbl_mainPanel);
		{
			JLabel lblNewLabel_2 = new JLabel("TH\u00CAM TR\u1EA0M XE");
			lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 14));
			GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
			gbc_lblNewLabel_2.anchor = GridBagConstraints.NORTH;
			gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
			gbc_lblNewLabel_2.gridx = 1;
			gbc_lblNewLabel_2.gridy = 0;
			mainPanel.add(lblNewLabel_2, gbc_lblNewLabel_2);
		}
		{
			JLabel lblNewLabel = new JLabel("T\u00EAn tr\u1EA1m:");
			GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
			gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
			gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
			gbc_lblNewLabel.gridx = 0;
			gbc_lblNewLabel.gridy = 1;
			mainPanel.add(lblNewLabel, gbc_lblNewLabel);
		}
		{
			nameField = new JTextField();
			GridBagConstraints gbc_nameField = new GridBagConstraints();
			gbc_nameField.anchor = GridBagConstraints.NORTH;
			gbc_nameField.fill = GridBagConstraints.HORIZONTAL;
			gbc_nameField.insets = new Insets(0, 0, 5, 5);
			gbc_nameField.gridx = 1;
			gbc_nameField.gridy = 1;
			mainPanel.add(nameField, gbc_nameField);
			nameField.setColumns(10);
		}
		{
			nameAlert = new JLabel("* Th\u00EAm t\u00EAn tr\u1EA1m");
			nameAlert.setForeground(Color.RED);
			GridBagConstraints gbc_nameAlert = new GridBagConstraints();
			gbc_nameAlert.anchor = GridBagConstraints.NORTHWEST;
			gbc_nameAlert.insets = new Insets(0, 0, 5, 0);
			gbc_nameAlert.gridx = 2;
			gbc_nameAlert.gridy = 1;
			mainPanel.add(nameAlert, gbc_nameAlert);
			nameAlert.setVisible(false);
		}
		{
			JLabel lblNewLabel_1 = new JLabel("\u0110\u1ECBa ch\u1EC9:");
			GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
			gbc_lblNewLabel_1.anchor = GridBagConstraints.NORTHEAST;
			gbc_lblNewLabel_1.insets = new Insets(0, 0, 0, 5);
			gbc_lblNewLabel_1.gridx = 0;
			gbc_lblNewLabel_1.gridy = 2;
			mainPanel.add(lblNewLabel_1, gbc_lblNewLabel_1);
		}
		{
			addressEditor = new JEditorPane();
			GridBagConstraints gbc_addressEditor = new GridBagConstraints();
			gbc_addressEditor.insets = new Insets(0, 0, 0, 5);
			gbc_addressEditor.fill = GridBagConstraints.BOTH;
			gbc_addressEditor.gridx = 1;
			gbc_addressEditor.gridy = 2;
			mainPanel.add(addressEditor, gbc_addressEditor);
		}
		{
			addressAlert = new JLabel("*Th\u00EAm \u0111\u1ECBa ch\u1EC9");
			addressAlert.setForeground(Color.RED);
			GridBagConstraints gbc_addressAlert = new GridBagConstraints();
			gbc_addressAlert.anchor = GridBagConstraints.NORTHWEST;
			gbc_addressAlert.gridx = 2;
			gbc_addressAlert.gridy = 2;
			mainPanel.add(addressAlert, gbc_addressAlert);
			addressAlert.setVisible(false);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Thêm");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						boolean checkValid = true;
						String name = nameField.getText().trim();
						if(name.equals("")) {
							checkValid = false;
							nameAlert.setVisible(true);
						}
						String address = addressEditor.getText().trim();
						if(address.equals("")) {
							checkValid = false;
							addressAlert.setVisible(true);
						}
						if(checkValid) {
							Station station = new Station(null, name, address);
							if(StationApi.getInstance().addStation(station) == null) {
								JOptionPane.showMessageDialog(null, "Có lỗi xảy ra");
							}else {
								JOptionPane.showMessageDialog(null, "Thêm trạm mới thành công");
								dispose();
							}
						}
						
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Thoát");
				cancelButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				buttonPane.add(cancelButton);
			}
		}
	}

}
