package com.ebike.screen.station.gui;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;

import javax.swing.UIManager;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.ws.rs.core.NewCookie;

import com.ebike.screen.rental.controller.RentalController;

import com.ebike.bean.station.Station;
import com.ebike.screen.station.controller.StationController;
import com.ebike.serverapi.station.StationApi;

import javax.swing.JLayeredPane;
import javax.swing.BoxLayout;
import java.awt.GridBagLayout;
import java.awt.CardLayout;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.ScrollPane;
import java.awt.Panel;
import java.awt.List;
import javax.swing.Box;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

@SuppressWarnings("serial")
public class AdminStationListPanel extends JPanel
{
	
	private ArrayList<Station> stations;
	private StationApi stationApi;
	private AdminStationSingleItem adminStationSingleItem;
	private StationController controller;
	private JTextField textField;
	private JPanel  panel_1;
	
	/**
	 * Create the panel.
	 */

	
	public AdminStationListPanel(StationController stationController) {
		
		this.controller=stationController;
		
		stations=new ArrayList<Station>();
		stations=StationApi.getInstance().getAllStation();
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 1, 772, 75);
		add(panel);
		panel.setLayout(null);
	
		textField = new JTextField();
		
		textField.setBounds(12, 25, 432, 25);
		panel.add(textField);
		textField.setColumns(10);
		textField.setText("Tìm kiếm...");
		textField.setForeground(Color.GRAY);
		textField.addFocusListener(new FocusListener() {
		    @Override
		    public void focusGained(FocusEvent e) {
		        if (textField.getText().equals("Tìm kiếm...")) {
		        	textField.setText("");
		        	textField.setForeground(Color.BLACK);
		        }
		    }
		    @Override
		    public void focusLost(FocusEvent e) {
		        if (textField.getText().isEmpty()) {
		        	textField.setForeground(Color.GRAY);
		        	textField.setText("Tìm kiếm...");
		        }
		    }
		   });
		
		JButton btnTmKim = new JButton("Làm mới");
		btnTmKim.setBounds(456, 25, 90, 25);
		panel.add(btnTmKim);
		btnTmKim.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				updateData(StationApi.getInstance().getAllStation());
			}
		});
		JButton addStation = new JButton("Th\u00EAm M\u1EDBi");
		addStation.setBounds(643, 25, 97, 25);
		panel.add(addStation);
		addStation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.ShowAddStationDialog();
			}
		});
		panel.add(Box.createRigidArea(new Dimension(5, 0)));

		JLabel lblDanhSchBi_1 = new JLabel("Danh S\u00E1ch B\u00E3i \u0110\u1ED7 Xe");
		lblDanhSchBi_1.setBounds(10, 77, 772, 21);
		lblDanhSchBi_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblDanhSchBi_1.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblDanhSchBi_1);
		
		
		
		
		panel_1 = new JPanel() {
			@Override
			public java.awt.Dimension getPreferredSize() {

			    int h = super.getPreferredSize().height;
			    int w = getParent().getSize().width;
			    return new java.awt.Dimension(w, h);
			}
		};
		
		
		panel_1.setLayout(new GridLayout(0, 2, 4, 4));
		
		
		for (Station station : stations) {
			adminStationSingleItem=new AdminStationSingleItem(station,controller);
			adminStationSingleItem.setPreferredSize(new Dimension(300, 200));
			panel_1.add(adminStationSingleItem);
		}
		
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);
		scrollPane.setBounds(12, 112, 763, 345);
		
		add(scrollPane);
	
		scrollPane.setViewportView(panel_1);

		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				
				
				ArrayList<Station> stationName,stationAddress;
				
				Map<String,String> mapName=new HashMap<String,String>();
				mapName.put("name",textField.getText());
				stationName=StationApi.getInstance().getStations(mapName);
				
				Map<String,String> mapAddress=new HashMap<String,String>();
				mapAddress.put("address",textField.getText());
				stationAddress=StationApi.getInstance().getStations(mapAddress);
				
				for (Station station : stationName) {
					if(stationAddress.indexOf(station)==-1) {
						stationAddress.add(station);
					}
				}
				
				updateData(stationAddress);
				
			}
		});
	
	}
	
	public void updateData(ArrayList<Station> stations) {
		panel_1.removeAll();
		panel_1.revalidate();
		panel_1.repaint();
		
		for (Station station : stations) {
			adminStationSingleItem=new AdminStationSingleItem(station,controller);
			adminStationSingleItem.setPreferredSize(new Dimension(300, 200));
			panel_1.add(adminStationSingleItem);
		}
	}
	
	  
}
