package com.ebike.screen.station.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.ebike.bean.bike.Bike;
import com.ebike.bean.station.Station;
import com.ebike.serverapi.bike.BikeApi;
import com.ebike.serverapi.station.StationApi;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import javax.swing.JComboBox;

public class ChangeStationDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private Bike bike;
	private Station srcStation;
	private Station dstStation;
	private String stationId;
	
	JLabel dstStationId;
	JLabel dstStationName;
	
	JComboBox<ComboItem> comboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ChangeStationDialog dialog = new ChangeStationDialog(BikeApi.getInstance().getBike("2", "4"), StationApi.getInstance().getStations(null).get(1));
			dialog.setVisible(true);
			String rString = dialog.showDialog();
			System.out.println(rString);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ChangeStationDialog(Bike bike, Station station) {
		setModal(true);
		this.bike = bike;
		this.srcStation = station;
		setBounds(100, 100, 450, 244);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JLabel lblNewLabel = new JLabel("CHUY\u1EC2N TR\u1EA0M");
			lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
			lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
			contentPanel.add(lblNewLabel, BorderLayout.NORTH);
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.CENTER);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[] {10, 10, 10, 20, 10, 0, 10};
			gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
			gbl_panel.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, 1.0};
			gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				JLabel lblNewLabel_5 = new JLabel("Th\u00F4ng tin xe");
				lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 11));
				GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
				gbc_lblNewLabel_5.anchor = GridBagConstraints.WEST;
				gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_5.gridx = 0;
				gbc_lblNewLabel_5.gridy = 0;
				panel.add(lblNewLabel_5, gbc_lblNewLabel_5);
			}
			{
				JLabel lblNewLabel_1 = new JLabel("ID:");
				GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
				gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
				gbc_lblNewLabel_1.gridx = 0;
				gbc_lblNewLabel_1.gridy = 1;
				panel.add(lblNewLabel_1, gbc_lblNewLabel_1);
			}
			{
				JLabel bikeIdLabel = new JLabel(bike.getId());
				GridBagConstraints gbc_bikeIdLabel = new GridBagConstraints();
				gbc_bikeIdLabel.anchor = GridBagConstraints.WEST;
				gbc_bikeIdLabel.insets = new Insets(0, 0, 5, 5);
				gbc_bikeIdLabel.gridx = 1;
				gbc_bikeIdLabel.gridy = 1;
				panel.add(bikeIdLabel, gbc_bikeIdLabel);
			}
			{
				JLabel lblNewLabel_2 = new JLabel("T\u00EAn xe:");
				GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
				gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_2.anchor = GridBagConstraints.EAST;
				gbc_lblNewLabel_2.gridx = 2;
				gbc_lblNewLabel_2.gridy = 1;
				panel.add(lblNewLabel_2, gbc_lblNewLabel_2);
			}
			{
				JLabel bikeNameLabel = new JLabel(bike.getName());
				GridBagConstraints gbc_bikeNameLabel = new GridBagConstraints();
				gbc_bikeNameLabel.anchor = GridBagConstraints.WEST;
				gbc_bikeNameLabel.insets = new Insets(0, 0, 5, 5);
				gbc_bikeNameLabel.gridx = 3;
				gbc_bikeNameLabel.gridy = 1;
				panel.add(bikeNameLabel, gbc_bikeNameLabel);
			}
			{
				JLabel lblNewLabel_3 = new JLabel("Lo\u1EA1i:");
				GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
				gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_3.anchor = GridBagConstraints.EAST;
				gbc_lblNewLabel_3.gridx = 4;
				gbc_lblNewLabel_3.gridy = 1;
				panel.add(lblNewLabel_3, gbc_lblNewLabel_3);
			}
			{
				JLabel bikeClassLabel = new JLabel(bike.getClass().getSimpleName());
				GridBagConstraints gbc_bikeClassLabel = new GridBagConstraints();
				gbc_bikeClassLabel.anchor = GridBagConstraints.WEST;
				gbc_bikeClassLabel.insets = new Insets(0, 0, 5, 5);
				gbc_bikeClassLabel.gridx = 5;
				gbc_bikeClassLabel.gridy = 1;
				panel.add(bikeClassLabel, gbc_bikeClassLabel);
			}
			{
				JLabel lblNewLabel_4 = new JLabel("Tr\u1EA1m ngu\u1ED3n");
				lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 11));
				GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
				gbc_lblNewLabel_4.anchor = GridBagConstraints.WEST;
				gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_4.gridx = 0;
				gbc_lblNewLabel_4.gridy = 2;
				panel.add(lblNewLabel_4, gbc_lblNewLabel_4);
			}
			{
				JLabel lblNewLabel_6 = new JLabel("ID:");
				lblNewLabel_6.setHorizontalAlignment(SwingConstants.TRAILING);
				GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
				gbc_lblNewLabel_6.anchor = GridBagConstraints.EAST;
				gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_6.gridx = 0;
				gbc_lblNewLabel_6.gridy = 3;
				panel.add(lblNewLabel_6, gbc_lblNewLabel_6);
			}
			{
				JLabel srcStationId = new JLabel(srcStation.getId());
				GridBagConstraints gbc_srcStationId = new GridBagConstraints();
				gbc_srcStationId.anchor = GridBagConstraints.WEST;
				gbc_srcStationId.insets = new Insets(0, 0, 5, 5);
				gbc_srcStationId.gridx = 1;
				gbc_srcStationId.gridy = 3;
				panel.add(srcStationId, gbc_srcStationId);
			}
			{
				JLabel lblNewLabel_7 = new JLabel("T\u00EAn tr\u1EA1m:");
				GridBagConstraints gbc_lblNewLabel_7 = new GridBagConstraints();
				gbc_lblNewLabel_7.anchor = GridBagConstraints.EAST;
				gbc_lblNewLabel_7.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_7.gridx = 2;
				gbc_lblNewLabel_7.gridy = 3;
				panel.add(lblNewLabel_7, gbc_lblNewLabel_7);
			}
			{
				JLabel srcStationName = new JLabel(srcStation.getName());
				GridBagConstraints gbc_srcStationName = new GridBagConstraints();
				gbc_srcStationName.insets = new Insets(0, 0, 5, 0);
				gbc_srcStationName.anchor = GridBagConstraints.WEST;
				gbc_srcStationName.gridwidth = 4;
				gbc_srcStationName.gridx = 3;
				gbc_srcStationName.gridy = 3;
				panel.add(srcStationName, gbc_srcStationName);
			}
			{
				JLabel lblNewLabel_8 = new JLabel("Tr\u1EA1m \u0111\u00EDch");
				lblNewLabel_8.setFont(new Font("Tahoma", Font.BOLD, 11));
				GridBagConstraints gbc_lblNewLabel_8 = new GridBagConstraints();
				gbc_lblNewLabel_8.anchor = GridBagConstraints.WEST;
				gbc_lblNewLabel_8.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_8.gridx = 0;
				gbc_lblNewLabel_8.gridy = 4;
				panel.add(lblNewLabel_8, gbc_lblNewLabel_8);
			}
			{
				JLabel lblNewLabel_10 = new JLabel("ID:");
				GridBagConstraints gbc_lblNewLabel_10 = new GridBagConstraints();
				gbc_lblNewLabel_10.anchor = GridBagConstraints.EAST;
				gbc_lblNewLabel_10.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_10.gridx = 0;
				gbc_lblNewLabel_10.gridy = 5;
				panel.add(lblNewLabel_10, gbc_lblNewLabel_10);
			}
			{
				dstStationId = new JLabel(" ");
				GridBagConstraints gbc_dstStationId = new GridBagConstraints();
				gbc_dstStationId.anchor = GridBagConstraints.WEST;
				gbc_dstStationId.insets = new Insets(0, 0, 5, 5);
				gbc_dstStationId.gridx = 1;
				gbc_dstStationId.gridy = 5;
				panel.add(dstStationId, gbc_dstStationId);
			}
			{
				JLabel lblNewLabel_11 = new JLabel("T\u00EAn tr\u1EA1m:");
				GridBagConstraints gbc_lblNewLabel_11 = new GridBagConstraints();
				gbc_lblNewLabel_11.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_11.gridx = 2;
				gbc_lblNewLabel_11.gridy = 5;
				panel.add(lblNewLabel_11, gbc_lblNewLabel_11);
			}
			{
				dstStationName = new JLabel(" ");
				GridBagConstraints gbc_dstStationName = new GridBagConstraints();
				gbc_dstStationName.insets = new Insets(0, 0, 5, 0);
				gbc_dstStationName.anchor = GridBagConstraints.WEST;
				gbc_dstStationName.gridwidth = 4;
				gbc_dstStationName.gridx = 3;
				gbc_dstStationName.gridy = 5;
				panel.add(dstStationName, gbc_dstStationName);
			}
			{
				JLabel lblNewLabel_9 = new JLabel("Ch\u1ECDn tr\u1EA1m \u0111\u00EDch:");
				GridBagConstraints gbc_lblNewLabel_9 = new GridBagConstraints();
				gbc_lblNewLabel_9.anchor = GridBagConstraints.EAST;
				gbc_lblNewLabel_9.insets = new Insets(0, 0, 0, 5);
				gbc_lblNewLabel_9.gridx = 0;
				gbc_lblNewLabel_9.gridy = 6;
				panel.add(lblNewLabel_9, gbc_lblNewLabel_9);
			}
			{
				comboBox = new JComboBox<ComboItem>();
				ArrayList<Station> listStations = StationApi.getInstance().getStations(null);
				listStations.remove(srcStation);
				for (Station s : listStations) {
					comboBox.addItem(new ComboItem(s.getName(), s.getId()));
				}
				comboBox.addActionListener (new ActionListener () {
				    public void actionPerformed(ActionEvent e) {
				       ComboItem item = (ComboItem) comboBox.getSelectedItem();
				       dstStationId.setText(item.getValue());
				       dstStationName.setText(item.getKey());
				    }
				});
				GridBagConstraints gbc_comboBox = new GridBagConstraints();
				gbc_comboBox.gridwidth = 6;
				gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
				gbc_comboBox.gridx = 1;
				gbc_comboBox.gridy = 6;
				panel.add(comboBox, gbc_comboBox);
				
				ComboItem item = (ComboItem) comboBox.getSelectedItem();
		        dstStationId.setText(item.getValue());
		        dstStationName.setText(item.getKey());
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Chuy\u1EC3n");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						stationId = dstStationId.getText();
						if(stationId.trim().equals("")) stationId = null;
						setVisible(false);
						dispose();
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Tho\u00E1t");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						stationId = null;
						setVisible(false);
						dispose();
					}
				});
				buttonPane.add(cancelButton);
			}
		}
	}
	
	String showDialog() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setVisible(true);
		setLocationRelativeTo(null);
		return stationId;
	}
	
	class ComboItem
	{
	    private String key;
	    private String value;

	    public ComboItem(String key, String value)
	    {
	        this.key = key;
	        this.value = value;
	    }

	    @Override
	    public String toString()
	    {
	        return key;
	    }

	    public String getKey()
	    {
	        return key;
	    }

	    public String getValue()
	    {
	        return value;
	    }
	}
}
