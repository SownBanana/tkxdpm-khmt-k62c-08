package com.ebike.screen.station.gui;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import com.ebike.bean.station.Station;
import com.ebike.screen.station.controller.StationController;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class AdminStationSingleItem extends JPanel {

	private StationController controller;
	private Station station;
	/**
	 * Create the panel.
	 */
	public AdminStationSingleItem(Station station,StationController stationController) {
		
		
		
		this.station = station;
		this.controller=stationController;
		
		controller.getNumberBike(station.getBikes());
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setLayout(null);
		this.setSize(360, 200);
		JLabel labelName = new JLabel(station.getName());
	
		labelName.setBounds(0, 0, 358, 40);
		add(labelName);
		labelName.setHorizontalAlignment(SwingConstants.CENTER);
		
		JTextPane txtpnaCh = new JTextPane();
		txtpnaCh.setEditable(false);
		txtpnaCh.setBackground(UIManager.getColor("Button.background"));
		txtpnaCh.setText("\u0110\u1ECBa Ch\u1EC9: "+station.getAddress());
		txtpnaCh.setBounds(10, 37, 158, 56);
		add(txtpnaCh);

		
		JLabel labelTwinBike = new JLabel("S\u1ED1 L\u01B0\u1EE3ng Xe \u0110\u1EA1p \u0110\u00F4i:"+controller.getNumberTwinBike());
		labelTwinBike.setBounds(12, 104, 199, 16);
		add(labelTwinBike);
		
		JLabel labelRegularBike = new JLabel("S\u1ED1 L\u01B0\u1EE3ng Xe \u0110\u1EA1p:"+controller.getNumberRegularBike());
		labelRegularBike.setBounds(12, 141, 199, 16);
		add(labelRegularBike);
		
		BufferedImage img = null;
		try {
			 img = ImageIO.read(new File(StationSingleItem.class.getResource("/assets/station.jpg").getPath().toString()));
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
		JLabel labelImg = new JLabel("New label");
		labelImg.setHorizontalAlignment(SwingConstants.CENTER);
		labelImg.setBounds(190, 37, 158, 150);
		Image dimg = img.getScaledInstance(labelImg.getWidth(), labelImg.getHeight(),Image.SCALE_SMOOTH);
		ImageIcon imageIcon = new ImageIcon(dimg);
		labelImg.setIcon(imageIcon);
	
		add(labelImg);
		
		JLabel labelEBike = new JLabel("S\u1ED1 L\u01B0\u1EE3ng Xe \u0110\u1EA1p \u0110i\u1EC7n:"+controller.getNumberEBike());
		labelEBike.setBounds(12, 122, 199, 16);
		add(labelEBike);
		
		this.setMaximumSize(new Dimension(360, 200));
		this.setMinimumSize(new Dimension(360, 200));
		
		JButton editBikesBtn = new JButton("Chỉnh Sửa");
		editBikesBtn.setBounds(10, 162, 158, 25);
		add(editBikesBtn);
		editBikesBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.ShowEditStationDialog(station);
			}
		});
	
	

	}
	
	 

	public void setController(StationController stationController){
	        this.controller=stationController;
	    }
}
