package com.ebike.screen.bike.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import com.ebike.bean.bike.Bike;
import com.ebike.bean.station.Station;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class BikeInStationFrame extends JFrame {

	private JPanel contentPane;
	private JTextField searchField;
	private BikeListPane listBike;
	private Station station;
	
	private String searchMode = "Tất cả";

	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 */
	public BikeInStationFrame(Station station) {
		this.station = station;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 514, 374);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 10));
		setContentPane(contentPane);
		
		JPanel searchPanel = new JPanel();
		contentPane.add(searchPanel, BorderLayout.NORTH);
		searchPanel.setLayout(new BoxLayout(searchPanel, BoxLayout.X_AXIS));
		searchField = new JTextField();
		searchField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				String param = searchField.getText().trim().toLowerCase();
				searchBike(searchMode, param);
			}
		});
		searchPanel.add(searchField);
		searchPanel.add(Box.createRigidArea(new Dimension(5, 0)));
		searchField.setColumns(10);
		
		
		listBike = new BikeListPane(station.getBikes());
		contentPane.add(listBike, BorderLayout.CENTER);
		
		
		JComboBox comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				searchMode = (String) comboBox.getSelectedItem();
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Tất cả", "Tên xe", "Loại xe", "ID", "Mô tả", "Hãng", "Biển số xe"}));
		searchPanel.add(comboBox);
	}
	
	public void updateData(ArrayList<Bike> bikeList) {
		this.listBike.updateData(bikeList);
	}
	
	public ArrayList<Bike> searchBike(String searchMode, String param) {
		
		ArrayList<Bike> list = new ArrayList<Bike>();
		Bike bike = BikeSearchFactory.singleton().createBike(new HashMap<String, String>(Collections.singletonMap(searchMode, param)));
		for (Bike b : station.getBikes()) {
			if(b.matchOnce(bike)) list.add(b);
			else if(searchMode.equals("Tất cả") || searchMode.equals("Loại xe")) {
				if(b.getClass().getSimpleName().toLowerCase().contains(param.toLowerCase())) list.add(b);
			}
		}
		listBike.updateData(list);
		return list;
	}
}
