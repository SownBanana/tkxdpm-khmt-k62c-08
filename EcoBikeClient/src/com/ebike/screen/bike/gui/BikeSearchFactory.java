package com.ebike.screen.bike.gui;

import java.util.HashMap;

import com.ebike.bean.bike.Bike;

public class BikeSearchFactory {
	private static BikeSearchFactory singleton;
	
	public static BikeSearchFactory singleton(){
		if (singleton == null){
			synchronized(BikeSearchFactory.class){
				if (singleton == null){
					singleton = new BikeSearchFactory();
				}
			}
		}
		return singleton;
	}
	

	public Bike createBike(HashMap<String, String> map) {
		Bike bike = new Bike();
		for (String type : map.keySet()) {
			String param = map.get(type);
			switch (type) {
			case "Tất cả":
				bike.setId(param);
				bike.setName(param);
				bike.setDescription(param);
				bike.setBrand(param);
				bike.setLicence(param);
				break;
			case "ID":
				bike.setId(param);
				break;
			case "Tên xe":
				bike.setName(param);
				break;
			case "Mô tả":
				bike.setDescription(param);
				break;
			case "Hãng":
				bike.setBrand(param);
				break;
			case "Biển số xe":
				bike.setLicence(param);
				break;
			default:
				return null;
			}
		}
		return bike;
	}
}
