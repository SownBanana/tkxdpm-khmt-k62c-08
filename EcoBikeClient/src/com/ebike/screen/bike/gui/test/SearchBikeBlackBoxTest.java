package com.ebike.screen.bike.gui.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.ebike.bean.bike.Bike;
import com.ebike.bean.bike.EBike;
import com.ebike.bean.bike.RegularBike;
import com.ebike.bean.bike.TwinBike;
import com.ebike.bean.station.Station;
import com.ebike.screen.bike.gui.BikeInStationFrame;

@RunWith(Parameterized.class)
public class SearchBikeBlackBoxTest {
	private String searchMode;
	private String param;
	private int expectedBikeNumber;

	
	
	public SearchBikeBlackBoxTest(String searchMode, String param, int expectedBookNumber) {
		super();
		this.searchMode = searchMode;
		this.param = param;
		this.expectedBikeNumber = expectedBookNumber;
	}
	
	
	@Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][] { 
			// Test case 1
			{ "ID", "2A", 0},
			// Test case 2
			{ "Tên xe", "Xe thứ", 3}, 
			// Test case 3
			{ "Mô tả", "CUỐI CÙNG", 1},
			// Test case 4
			{ "Hãng", "Thống Nhất", 2},
			// Test case 5
			{ "Biển số xe", "B34", 0},
			// Test case 6
			{ "Loại xe", "regularBike", 2},
			//Test case 7
			{ "Tất cả", "Thứ Nhất", 2},
			// Test case 8
			{ "ID", "2", 1},
			// Test case 9
			{ "Tên xe", "xe", 4}, 
			// Test case 10
			{ "Mô tả", "chiếc xe", 2},
			// Test case 11
			{ "Hãng", "asama", 2},
			// Test case 12
			{ "Biển số xe", "34", 2},
			// Test case 13
			{ "Loại xe", "ebike", 1},
			// Test case 14
			{ "Tất cả", "thứ nhất", 2}
			
		});
	}

	@Test
	public void searchTest() {
		ArrayList<Bike> list = SearchBikeTestSuit.framedump.searchBike(searchMode, param);
		assertEquals("Error " + searchMode, expectedBikeNumber,list.size());
	}
}
