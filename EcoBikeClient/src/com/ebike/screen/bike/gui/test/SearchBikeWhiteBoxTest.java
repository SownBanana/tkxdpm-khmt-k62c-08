package com.ebike.screen.bike.gui.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.ebike.bean.bike.Bike;

@RunWith(Parameterized.class)
public class SearchBikeWhiteBoxTest {
	private String searchMode;
	private String param;
	private int expectedBikeNumber;

	public SearchBikeWhiteBoxTest(String searchMode, String param, int expectedBookNumber) {
		super();
		this.searchMode = searchMode;
		this.param = param;
		this.expectedBikeNumber = expectedBookNumber;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][] { 
			// Test case 1
			{ "Tất cả", "Xe thứ", 3},
			// Test case 2
			{ "ID", "", 4},
			// Test case 3
			{ "Tên xe", "Xe", 4}, 
			// Test case 4
			{ "Mô tả", "xe", 4},
			// Test case 5
			{ "Hãng", "Thống Nhất", 2},
			// Test case 6
			{ "Biển số xe", "34", 2},
			//Test case 7
			{ "Loại xe", "regularBike", 2}
			
		});
	}

	@Test
	public void searchTest() {
		ArrayList<Bike> list = SearchBikeTestSuit.framedump.searchBike(searchMode, param);
		assertEquals("Error " + searchMode, expectedBikeNumber,list.size());
	}
}
