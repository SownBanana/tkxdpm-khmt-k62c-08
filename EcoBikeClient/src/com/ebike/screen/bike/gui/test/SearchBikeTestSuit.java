package com.ebike.screen.bike.gui.test;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.ebike.bean.bike.Bike;
import com.ebike.bean.bike.EBike;
import com.ebike.bean.bike.RegularBike;
import com.ebike.bean.bike.TwinBike;
import com.ebike.bean.station.Station;
import com.ebike.screen.bike.gui.BikeInStationFrame;

@RunWith(Suite.class)
@SuiteClasses({ SearchBikeBlackBoxTest.class, SearchBikeWhiteBoxTest.class })
public class SearchBikeTestSuit {
	public static BikeInStationFrame framedump;

	@BeforeClass 
    public static void setUpClass() {      
		ArrayList<Bike> examples = new ArrayList<Bike>();
		Bike bike1 = new RegularBike("1", "Xe thứ nhất", "Đây là chiếc xe thứ nhất", "Thống Nhất", "1234");
		examples.add(bike1);
		Bike bike2 = new EBike("2", "Xe thứ hai", "Xe này là thứ hai sau xe thứ nhất", "Asama", "2345");
		examples.add(bike2);
		Bike bike3 = new TwinBike("3", "Xe ba", "Đây là chiếc xe ba","Thống Nhất", "5790");
		examples.add(bike3);
		Bike bike4 = new RegularBike("4", "Xe thứ tư", "Xe cuối cùng", "Asama","6542");
		examples.add(bike4);
		Station bikeHolder = new Station();
		bikeHolder.setBikes(examples);
		framedump = new BikeInStationFrame(bikeHolder);
    }
}
