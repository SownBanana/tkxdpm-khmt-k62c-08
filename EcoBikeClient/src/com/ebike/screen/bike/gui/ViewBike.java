package com.ebike.screen.bike.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.ebike.bean.bike.Bike;

import javax.swing.JLabel;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.JButton;

import com.ebike.screen.rental.controller.RentalController;
import com.ebike.serverapi.bike.BikeApi;

import java.awt.Font;
import java.util.HashMap;

public class ViewBike extends JFrame {

	private JPanel contentPane;

	private static ViewBike viewBike=new ViewBike();

	public static ViewBike getInstance(){
		return viewBike;
	}


	/**
	 * Create the frame.
	 */
	public void ShowView(Bike bike){
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 303);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNamBike = new JLabel(bike.getName());
		lblNamBike.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNamBike.setHorizontalAlignment(SwingConstants.CENTER);
		lblNamBike.setBounds(58, 18, 296, 16);
		contentPane.add(lblNamBike);

		JLabel lblBikeImage = new JLabel("Image");
		lblBikeImage.setBounds(235, 56, 163, 163);
		contentPane.add(lblBikeImage);

		BufferedImage img = null;
		try {
			img = ImageIO.read(new File(ViewBike.class.getResource("/assets/bike.jpg").getPath()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Image dimg = img.getScaledInstance(lblBikeImage.getWidth(), lblBikeImage.getHeight(),
				Image.SCALE_SMOOTH);

		lblBikeImage.setIcon(new ImageIcon(dimg));


		JLabel lblBikeClass = new JLabel(bike.genBikeType());
		lblBikeClass.setBounds(102, 56, 91, 16);
		contentPane.add(lblBikeClass);

		JLabel lblBikeClassLabel = new JLabel("Loại xe:");
		lblBikeClassLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblBikeClassLabel.setBounds(29, 56, 61, 16);
		contentPane.add(lblBikeClassLabel);

		JLabel lblBikeDescriptionLabel = new JLabel("Miêu tả:");
		lblBikeDescriptionLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblBikeDescriptionLabel.setBounds(29, 106, 61, 16);
		contentPane.add(lblBikeDescriptionLabel);

		JLabel lblBikeDescription = new JLabel(bike.getDescription());
		lblBikeDescription.setBounds(102, 106, 91, 16);
		contentPane.add(lblBikeDescription);

		JLabel lblBikeBrandLabel = new JLabel("Hãng:");
		lblBikeBrandLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblBikeBrandLabel.setBounds(29, 146, 61, 16);
		contentPane.add(lblBikeBrandLabel);

		JLabel lblBikeBrand = new JLabel(bike.getBrand());
		lblBikeBrand.setBounds(102, 146, 91, 16);
		contentPane.add(lblBikeBrand);

		JLabel lblBikeLicenseLabel = new JLabel("Biển số");
		lblBikeLicenseLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblBikeLicenseLabel.setBounds(29, 281, 61, 16);
		contentPane.add(lblBikeLicenseLabel);

		JLabel lblBikeLicense = new JLabel(bike.getLicence());
		lblBikeLicense.setBounds(102, 185, 91, 16);
		contentPane.add(lblBikeLicense);

		JButton btnRentalButton = new JButton("Thuê xe");
		btnRentalButton.setBounds(29, 223, 117, 29);
		btnRentalButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				RentalController.getInstace().addRental(bike);
			}

		});
		contentPane.add(btnRentalButton);

		JLabel lblNewLabel = new JLabel("Biển số:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(44, 186, 46, 14);
		contentPane.add(lblNewLabel);
		setLocationRelativeTo(null);
		setVisible(true);
	}
}