package com.ebike.screen.bike.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.ebike.bean.bike.Bike;
import com.ebike.serverapi.bike.BikeApi;

import java.awt.GridLayout;


public class BikeListPane extends JScrollPane {
	protected JPanel pane;
	/**
	 * Create the panel.
	 */
	public BikeListPane(List<Bike> list) {
		pane = new JPanel() {
			@Override
			public java.awt.Dimension getPreferredSize() {

			    int h = super.getPreferredSize().height;
			    int w = getParent().getSize().width;
			    return new java.awt.Dimension(w, h);
			}
		};
		this.setViewportView(pane);
		pane.setLayout(new GridLayout(0, 2, 0, 0));
		this.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.getVerticalScrollBar().setUnitIncrement(20);
		this.getHorizontalScrollBar().setUnitIncrement(20);
		updateData(list);
	}
	public void updateData(List<Bike> list) {
		pane.removeAll();
		pane.revalidate();
		pane.repaint();
		
		for (Bike b: list) {
		    BikeSingleItem singlePane = new BikeSingleItem(b);
		    singlePane.setPreferredSize(new Dimension(300, 100));
		    pane.add(singlePane);
        }
	}
	
	public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
		JFrame frame = new JFrame();
		frame.getContentPane().add(new BikeListPane(BikeApi.getInstance().getBikes(null, null)), BorderLayout.CENTER);
		frame.setSize(700, 500);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

}
