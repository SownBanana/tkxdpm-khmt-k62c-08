package com.ebike.screen.bike.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ManageBike extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ManageBike frame = new ManageBike();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ManageBike() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(116, 26, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(116, 57, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(116, 88, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(116, 126, 86, 44);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JTextArea txtrName = new JTextArea();
		txtrName.setText("Name");
		txtrName.setBounds(22, 24, 48, 22);
		contentPane.add(txtrName);
		
		JTextArea txtrBrand = new JTextArea();
		txtrBrand.setText("Brand");
		txtrBrand.setBounds(22, 55, 47, 22);
		contentPane.add(txtrBrand);
		
		JTextArea txtrLicence = new JTextArea();
		txtrLicence.setText("Licence");
		txtrLicence.setBounds(22, 91, 57, 22);
		contentPane.add(txtrLicence);
		
		JTextArea txtrDescription = new JTextArea();
		txtrDescription.setText("Description");
		txtrDescription.setBounds(24, 136, 93, 22);
		contentPane.add(txtrDescription);
		
		JButton btnAddBike = new JButton("Th�m xe");
		btnAddBike.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnAddBike.setBounds(116, 197, 89, 23);
		contentPane.add(btnAddBike);
		
		JLabel lblNewLabel = new JLabel("New label");
		BufferedImage img = null;
		lblNewLabel.setBounds(239, 26, 158, 144);
		try {
		    img = ImageIO.read(new File(BikeSingleItem.class.getResource("/assets/bikeicon.png").getPath().toString()));
		} catch (IOException e) {
		    e.printStackTrace();
		}
		Image dimg = img.getScaledInstance(lblNewLabel.getWidth(), lblNewLabel.getHeight(),
		        Image.SCALE_SMOOTH);
		
		lblNewLabel.setIcon(new ImageIcon(dimg));
		contentPane.add(lblNewLabel);
	}
}
