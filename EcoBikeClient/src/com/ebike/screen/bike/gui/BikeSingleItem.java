package com.ebike.screen.bike.gui;

import javax.swing.JPanel;

import com.ebike.screen.rental.controller.RentalController;
import com.ebike.serverapi.bike.BikeApi;
import com.ebike.bean.bike.Bike;

import javax.swing.JLabel;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.swing.border.TitledBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BikeSingleItem extends JPanel {
	Bike bike;
	/**
	 * Create the panel.
	 */
	public BikeSingleItem(Bike bike) {
		this.bike = bike;
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ViewBike.getInstance().ShowView(bike);
			}
			 public void mouseEntered(java.awt.event.MouseEvent evt) {
		        setBackground(new Color(220, 220, 220));
			 }

			 public void mouseExited(java.awt.event.MouseEvent evt) {
		        setBackground(new Color(240, 240, 240));
			 }
		});
		setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		setLayout(null);
		
		JLabel imageLabel = new JLabel("New label");
		imageLabel.setBounds(10, 11, 78, 78);
		BufferedImage img = null;
		try {
		    img = ImageIO.read(new File(BikeSingleItem.class.getResource("/assets/bikeicon.png").getPath().toString()));
		} catch (IOException e) {
		    e.printStackTrace();
		}
		Image dimg = img.getScaledInstance(imageLabel.getWidth(), imageLabel.getHeight(),
		        Image.SCALE_SMOOTH);
		
		imageLabel.setIcon(new ImageIcon(dimg));
		add(imageLabel);
		
		JLabel nameLabel = new JLabel(bike.getName());
		nameLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		nameLabel.setBounds(98, 19, 199, 23);
		add(nameLabel);
		
		JLabel typeLabel = new JLabel(bike.genBikeType());
		typeLabel.setBounds(98, 53, 103, 14); 
		add(typeLabel);
		
		JButton rentButton = new JButton("Thu� xe");
		rentButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RentalController.getInstace().addRental(bike);
			}
		});
		rentButton.setBounds(211, 49, 89, 23);
		add(rentButton);

	}
	
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.getContentPane().add(new BikeSingleItem(BikeApi.getInstance().getBikes(null, null).get(0)), BorderLayout.CENTER);
		frame.setSize(300, 300);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
