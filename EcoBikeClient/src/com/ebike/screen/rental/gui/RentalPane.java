package com.ebike.screen.rental.gui;

import com.ebike.screen.rental.controller.RentalController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class RentalPane  extends JPanel {
    private JLabel rentalStatusLabel;
    private RentalController rentalController;

    private static RentalPane rentalPane=new RentalPane();
    public static RentalPane getInstance(){
        return rentalPane;
    }

    private RentalPane(){
        this.setLayout(new FlowLayout(FlowLayout.RIGHT));
        rentalStatusLabel = new JLabel();
        this.add(rentalStatusLabel);
        JButton paymentButton = new JButton("Thanh toan");
        this.add(paymentButton);
        JButton bikeButton = new JButton("Thong tin xe");
        this.add(bikeButton);
        paymentButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                rentalController.showRentalDialog();
            }
        });
        bikeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                rentalController.showViewBike();
            }
        });
    }


    public void setController(RentalController rentalController){
        this.rentalController=rentalController;
    }
    public void updateData(String text){
        rentalStatusLabel.setText(text);
    }
    public void hiddenButton(){

    }

}
