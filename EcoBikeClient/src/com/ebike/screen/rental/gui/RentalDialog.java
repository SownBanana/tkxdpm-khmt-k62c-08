package com.ebike.screen.rental.gui;

//import com.sun.org.apache.bcel.internal.generic.FADD;
import com.ebike.screen.rental.controller.RentalController;
import com.ebike.bean.price.Price;
import com.ebike.bean.price.PriceManager;
import com.ebike.bean.rental.RentalDetail;
import com.ebike.bean.station.Station;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
public class RentalDialog extends JDialog {

    private static RentalDialog  rentalDialog=new RentalDialog();
    private GridBagLayout layout;
    private GridBagConstraints c;
    private RentalController controller;

    public static RentalDialog getInstance(){
        return rentalDialog;
    }

    private RentalDialog(){
        layout=new GridBagLayout();
        this.setLayout(layout);
        this.setLocationRelativeTo(null);
        c=new GridBagConstraints();

        updateData(null);

    }

    public void setController(RentalController rentalController){
        this.controller=rentalController;
    }

    public void updateData(RentalDetail rentalDetail){
        this.getContentPane().removeAll();
        // in case: client is using bike
        if(rentalDetail!=null && rentalDetail.getBike()!=null){
            c.gridx=0;
            c.gridy=6;
            add(new JLabel("THONG TIN THUE XE"),c);

            c.gridx=0;
            c.gridy=7;
            add(new JLabel("Bat dau "),c);
            c.gridx=1;
            c.gridy=7;
            SimpleDateFormat model = new SimpleDateFormat("HH:mm dd-MM-yyyy");
            JSpinner spinnerStart = new JSpinner(new SpinnerDateModel());
            spinnerStart.setValue(rentalDetail.getStartDate());
            add(spinnerStart,c);

            c.gridx=0;
            c.gridy=8;
            add(new JLabel("Ket thuc "),c);
            c.gridx=1;
            c.gridy=8;
            JSpinner spinnerFinish = new JSpinner(new SpinnerDateModel());
            //spinnerFinish.setEditor(new JSpinner.DateEditor(spinnerFinish, model.toPattern()));
            if(rentalDetail.getFinishDate()==null){
                spinnerFinish.setValue(new Date());
            }else{
                spinnerFinish.setValue(rentalDetail.getFinishDate());
            }
            add(spinnerFinish,c);

            c.gridx=0;
            c.gridy=9;
            add(new JLabel("Loai Thue"),c);
            c.gridx=1;
            c.gridy=9;
            JTextField rentalType=new JTextField(15);
            rentalType.setText(rentalDetail.getRentalType());
            rentalType.setEditable(false);
            add(rentalType,c);
            Price price= controller.getPrice();
            double deposit=price.getDeposit();
            c.gridx=0;
            c.gridy=10;
            add(new JLabel("Tien coc"),c);
            c.gridx=1;
            c.gridy=10;
            JTextField rentalDeposit=new JTextField(15);
            rentalDeposit.setEditable(false);
            rentalDeposit.setText(deposit+"");
            add(rentalDeposit,c);

            double paymentTemp;
            if(controller.isReturnBike()){
                paymentTemp= controller.calculateTempPayment(rentalDetail.getStartDate(),rentalDetail.getFinishDate());
            }
            else{
                paymentTemp= controller.calculateTempPayment(rentalDetail.getStartDate(),new Date());
            }
            c.gridx=0;
            c.gridy=11;
            add(new JLabel("Tien thue tam tinh:"),c);
            c.gridx=1;
            c.gridy=11;
            JTextField payment=new JTextField(15);
            payment.setEditable(false);
            payment.setText(Math.abs(paymentTemp)+"");
            add(payment,c);

            double totalPayment=deposit-paymentTemp;
            c.gridx=0;
            c.gridy=12;
            add(new JLabel("Tien con lai:"),c);
            c.gridx=1;
            c.gridy=12;
            JTextField total=new JTextField(15);
            total.setEditable(false);
            total.setText(totalPayment+"");
            add(total,c);

            c.gridx=0;
            c.gridy=13;
            add(new JLabel("Chon bai xe va tien hanh thanh toan"),c);

            c.gridx=0;
            c.gridy=14;
            add(new JLabel("Bai xe:"),c);

            c.gridx=1;
            c.gridy=14;

            List<Station> stations=controller.loadStationHasFreeDocks();
            List<String> listStation=new ArrayList<>();
            for (Station station:stations
            ) {
                listStation.add(station.getName());
            }
            JComboBox stationOptions = new JComboBox(listStation.toArray());
            stationOptions.setEditable(true);
            // write action at here for stationOption
            if(!controller.isReturnBike())
                add(stationOptions,c);

            c.gridx=0;
            c.gridy=15;
            JButton button=new JButton("Thanh toan");
            if(stations.size()>0)
                add(button,c);
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(controller.isReturnBike()){
                        if(totalPayment>0)
                            controller.showBankInfoDialog(-totalPayment);
                        else
                            controller.showBankInfoDialog(totalPayment);
                    }
                    else {
                        int indexOfStation = stationOptions.getSelectedIndex();
                        String stationId = stations.get(indexOfStation).getId();
                        controller.returnBike(stationId, totalPayment);
                    }
                }
            });
        }else {
            c.gridx=0;
            c.gridy=5;
            add(new JLabel("KHONG CO XE NAO DUOC THUE"),c);
        }
        this.revalidate();
        this.repaint();
        this.pack();
        this.setResizable(false);
    }

    protected int getLastRowIndex(){
        layout.layoutContainer(this.getContentPane());
        int [][] dim=layout.getLayoutDimensions();
        int rows=dim[1].length;
        return rows;
    }
}
