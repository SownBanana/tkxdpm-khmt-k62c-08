package com.ebike.screen.rental.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.ebike.screen.rental.controller.RentalController;
import com.ebike.serverapi.bank.BankApi;
import com.ebike.bean.*;
import com.ebike.bean.price.Hours24Price;
import com.ebike.bean.price.NormalPrice;
import com.ebike.bean.price.Price;
import com.ebike.bean.price.PriceType;

import javax.swing.JLabel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;
public class RentBikeDialog extends JDialog {
	private JPanel buttonPane;
	private RentalController controller;
	private double normalFirstPrice,normalStepPrice,normalDeposit,return24h,expri24h,depo24h;
	private JLabel lblNewLabel_2 ;
	private JLabel lbFirstPrice ;
	private JLabel lblNewLabel_3;
	private JLabel lbPrice;
	private JLabel lblNewLabel_4;
	private JLabel lbRentPrice ;
	private JLabel lb24hprice;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel;
	JComboBox comboBox;
	/**
	 * Launch the application.
	 */
	private static RentBikeDialog instace=new RentBikeDialog();
	 public static RentBikeDialog getInstance() {
	        return instace;
	    }
	public static void main(String[] args) {
		try {
			RentBikeDialog dialog = new RentBikeDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public RentalController getController() {
		return controller;
	}

	public void setController(RentalController controller) {
		this.controller = controller;
	}

	/**
	 * Create the dialog.
	 */
	// List<Price>
	
	private RentBikeDialog() {
		setBounds(100, 100, 449, 261);



		double firstPrice=normalFirstPrice;
		double stepPrice=normalStepPrice;
		double totalPayment=normalDeposit;
		{
			buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String type="";
						if(("24h").equals(comboBox. getSelectedItem().toString())) {
							type=PriceType.HOUR;
						}else
							type=PriceType.NORMAL;
						controller.rentBikeDialogSuccess(type);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		 lblNewLabel_2 = new JLabel("Giá 30p đầu:");
		 lbFirstPrice = new JLabel(firstPrice+"");
		 lblNewLabel_3 = new JLabel("Mỗi 15p tiếp theo:");
		 lbPrice = new JLabel(stepPrice+"");

		 lblNewLabel_4 = new JLabel("Tiền cọc: ");

		 lbRentPrice = new JLabel(totalPayment+"");

		 comboBox = new JComboBox();

		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Bình thường", "24h"}));

		 lblNewLabel = new JLabel("Giá 24h:");

		 lb24hprice = new JLabel("200000");

		 lblNewLabel_1 = new JLabel("*Quá 12h không được hoàn tiền");
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(comboBox.getSelectedItem().toString().equals("24h")) {

					lb24hprice.setVisible(true);
					lblNewLabel_1.setVisible(true);
					lblNewLabel.setVisible(true);
					lblNewLabel_2.setText("Tiền hoàn trả sớm/tiếng: ");
					lblNewLabel_3.setText("Tiền phạt trả muộn/tiếng: ");
					lbFirstPrice.setText(return24h+"");
					lbPrice.setText(expri24h+"");
                    lbRentPrice.setText(depo24h+"");
				}else {
					lbFirstPrice.setText(normalFirstPrice+"");
					lbPrice.setText(normalStepPrice+"");
					lbRentPrice.setText(normalDeposit+"");
					lblNewLabel_2.setText("Giá 30p đầu: ");
					lblNewLabel_3.setText("Mỗi 15p tiếp theo:");
					lb24hprice.setVisible(false);
					lblNewLabel_1.setVisible(false);
					lblNewLabel.setVisible(false);
				}
			}
		});
		lblNewLabel_1.setForeground(Color.RED);
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(11, Short.MAX_VALUE)
					.addComponent(lblNewLabel_2)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lbRentPrice)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lbFirstPrice)
							.addGap(18)
							.addComponent(lblNewLabel_3)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(lbPrice)))
					.addGap(332))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(buttonPane, GroupLayout.PREFERRED_SIZE, 414, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel_4)
					.addContainerGap(381, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel)
					.addGap(18)
					.addComponent(lb24hprice)
					.addGap(18)
					.addComponent(lblNewLabel_1)
					.addContainerGap(158, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(340, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(41)
					.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_2)
						.addComponent(lbFirstPrice)
						.addComponent(lblNewLabel_3)
						.addComponent(lbPrice))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(lb24hprice)
							.addComponent(lblNewLabel_1))
						.addComponent(lblNewLabel))
					.addGap(8)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_4)
						.addComponent(lbRentPrice))
					.addPreferredGap(ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
					.addComponent(buttonPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		getContentPane().setLayout(groupLayout);
	}
	public void addlistPrice(List<Price> list) {
		for(Price price:list) {
			if(price instanceof NormalPrice) {
				normalFirstPrice= ((NormalPrice)price).getFirstPrice();
				normalStepPrice= ((NormalPrice)price).getStepPrice();
				normalDeposit=price.getDeposit();
				lbFirstPrice.setText(normalFirstPrice+"");
				lbPrice.setText(normalStepPrice+"");
				lbRentPrice.setText(normalDeposit+"");
				lblNewLabel_2.setText("Giá 30p đầu: ");
				lblNewLabel_3.setText("Mỗi 15p tiếp theo: ");
				lb24hprice.setVisible(false);
				lblNewLabel_1.setVisible(false);
				lblNewLabel.setVisible(false);
				
			}else if((price instanceof Hours24Price)) {
				return24h=((Hours24Price)price).getRefundStepPrice();
				expri24h=((Hours24Price)price).getOvertimeStepPrice();
				depo24h=price.getDeposit();
			}
		}
	}
}
