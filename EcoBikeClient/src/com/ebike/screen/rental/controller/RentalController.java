package com.ebike.screen.rental.controller;

import com.ebike.bean.bank.BankAccount;
import com.ebike.bean.bike.Bike;
import com.ebike.bean.price.Price;
import com.ebike.bean.price.PriceManager;
import com.ebike.bean.rental.RentalDetail;
import com.ebike.bean.station.Station;
import com.ebike.screen.bike.gui.ViewBike;
import com.ebike.screen.dialog.BankInfoDialog;
import com.ebike.screen.dialog.ShowMessage;
import com.ebike.screen.rental.gui.RentBikeDialog;
import com.ebike.screen.rental.gui.RentalDialog;
import com.ebike.screen.rental.gui.RentalPane;
import com.ebike.serverapi.bank.BankApi;
import com.ebike.serverapi.bank.IBankApi;
import com.ebike.serverapi.station.StationApi;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RentalController {

    private static RentalController rentalController = new RentalController();

    public static RentalController getInstace() {
        return rentalController;
    }

    // checkout
    private BankInfoDialog bankInfoDialog;

    // info rental
    private RentalPane rentalPane;
    private RentalDialog rentalDialog;
    private RentBikeDialog rentBikeDialog;
    private ViewBike viewBikeDetail;

    // data
    private RentalDetail rentalDetail;
    private Bike tempBike;
    private String tempTypePrice;

    private RentalController() {
        rentalDetail = null;
        rentalDialog = RentalDialog.getInstance();
        rentalPane = RentalPane.getInstance();
        bankInfoDialog = BankInfoDialog.getInstance();
        rentBikeDialog = RentBikeDialog.getInstance();
        viewBikeDetail = ViewBike.getInstance();

        rentalDialog.setController(this);
        rentalPane.setController(this);
        bankInfoDialog.setController(this);
        rentBikeDialog.setController(this);

        updateData(null);
    }

    public void showViewBike() {
        if (rentalDetail != null) {
            viewBikeDetail.ShowView(rentalDetail.getBike());
            viewBikeDetail.setVisible(true);
        } else
            ShowMessage.showInfoMessage("Khong co xe nao duoc thue");
    }

    //call function to rent a new Bike
    public void addRental(Bike bike) {
        // check condition before add rental : just only one bike in time
        if (this.rentalDetail != null) {
            ShowMessage.showErrorMessage("Xe khac dang duoc thue");
        } else {
            tempBike = bike;
            PriceManager priceManager=PriceManager.getInstance();
            List<Price> priceList = priceManager.getAllPriceByBikeType(bike.getBikeType());
            rentBikeDialog.addlistPrice(priceList);
            //call function on RentBikeDialog to show all type of price
            rentBikeDialog.setVisible(true);
        }
    }

    // call function when user click button ( choose type of rental)
    public void rentBikeDialogSuccess(String typePrice) {
        PriceManager priceManager = PriceManager.getInstance();
        Price price = priceManager.getPrice(typePrice, tempBike.getBikeType());
        tempTypePrice = typePrice;
        showBankInfoDialog(price.getDeposit());
    }

    // call function to transfer money
    // if payment>0: user send to system
    // if payment<=0: system send to user
    public void showBankInfoDialog(double payment) {
        bankInfoDialog.transfer(payment);
        bankInfoDialog.setVisible(true);
    }

    // payment >0 : user send money to system
    // payment<0  : system send money to user
    public void sendPayment(BankAccount user, double payment) {
        IBankApi bankApi = BankApi.getInstance();
        boolean checkTransfer = false;
        if (payment > 0) {
            checkTransfer = bankApi.transferUserToSystem(user, payment);
        } else
            checkTransfer = bankApi.transferSystemToUser(user, -payment);
        checkTransfer=true;
        if (checkTransfer) {
            transferSuccess();
        } else {
            ShowMessage.showErrorMessage("Chuyen khoan that bai");
        }
    }

    // call function when transfer payment success
    public void transferSuccess() {
        ShowMessage.showInfoMessage("Giao dich thanh cong");
        disableDialog();
        // in case: bike isn't still add to RentalDetail and user send deposit success
        if (tempBike != null) {
            RentalDetail newRentalDetail = new RentalDetail();
            newRentalDetail.setRentalType(tempTypePrice);
            newRentalDetail.setBike(tempBike);
            // check rent bike???
            rentBikeApi(tempBike.getId());

            tempBike = null;
            tempTypePrice = null;
            updateData(newRentalDetail);
        }
        // in case: user return bike success, and system send back money to user success
        else {
            updateData(null);
        }
    }

    // update data into Pane, dialog when rentalDetail change
    public void updateData(RentalDetail newRentalDetail) {
        rentalDetail = newRentalDetail;
        updateRentalPane();
        updateRentalDialog();
    }

    private void updateRentalPane() {
        rentalPane.updateData(getPresentationText());
    }

    private String getPresentationText() {
        if (rentalDetail != null && rentalDetail.getBike() != null)
            return "Dang thue xe: " + rentalDetail.getBike().getName();
        return "Chua thue xe!";
    }


    private void updateRentalDialog() {
        rentalDialog.updateData(rentalDetail);
    }

    // check condition if system received bike?
    public boolean isReturnBike() {
        if (rentalDetail.getFinishDate() == null)
            return false;
        else
            return true;
    }

    // return bike before checkout
    public void returnBike(String stationId, double payment) {
        if (returnBikeApi(stationId)) {
            ShowMessage.showInfoMessage("Nhan xe thanh cong");
            rentalDetail.setFinishDate(new Date());
            if (payment > 0)
                showBankInfoDialog(-payment);
            else
                showBankInfoDialog(payment);
        } else {
            ShowMessage.showErrorMessage("Nhan xe that bai");
        }
    }

    public boolean rentBikeApi(String bikeId) {
        StationApi stationApi = StationApi.getInstance();
        Bike bike = stationApi.rentBike(null, bikeId);
        if (bike.getId().equals(bikeId))
            return true;
        return false;
    }

    // save Bike to Station
    public boolean returnBikeApi(String stationId) {
        String bikeId = rentalDetail.getBike().getId();
        StationApi stationApi = StationApi.getInstance();
        try {
            Bike bikeResult = stationApi.returnBike(stationId, bikeId);
            if (bikeResult.getId().equals(bikeId))
                return true;
            return false;
        } catch (Exception ex) {
            return false;
        }
    }

    // function calculate temp price
    public double calculateTempPayment(Date start, Date finish) {
        try {
            return getPrice().calculatePrice(start, finish);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return 0;
        }
    }

    public Price getPrice(){
        Price price=rentalDetail.getPrice();
        return price;
    }

    public void showBikeViewDetail() {
        viewBikeDetail.setVisible(true);
    }

    public void disableBikeViewDetail() {
        viewBikeDetail.setVisible(false);
    }

    public void showRentalDialog() {
        updateData(rentalDetail);
        rentalDialog.setVisible(true);
    }

    public void disableRentalDialog() {
        rentalDialog.setVisible(false);
    }

    public void disableBankInfoDialog() {
        bankInfoDialog.setVisible(false);
    }

    public void disableDialog() {
        disableRentalDialog();
        disableBankInfoDialog();
        disableRentBikeDialog();
    }

    public void disableRentBikeDialog() {
        rentBikeDialog.setVisible(false);
    }


    public List<Station> loadStationHasFreeDocks() {
        System.out.println("LOAD STATION HAS FREE  DOC");
        try {
            return new StationApi().getStationHaveFreeDock();
        } catch (Exception ex) {
            System.err.print(ex.getMessage() + this.getClass());
            System.out.println("Error LOAD STATION API FREE DOC");
            return new ArrayList<>();
        }
    }

    public RentalPane getRentalPane() {
        return rentalPane;
    }

    public void setRentalPane(RentalPane rentalPane) {
        this.rentalPane = rentalPane;
    }

    public RentalDialog getRentalDialog() {
        return rentalDialog;
    }

    public void setRentalDialog(RentalDialog rentalDialog) {
        this.rentalDialog = rentalDialog;
    }

    public RentalDetail getRentalDetail() {
        return rentalDetail;
    }

    public void setRentalDetail(RentalDetail rentalDetail) {
        this.rentalDetail = rentalDetail;
    }

}
