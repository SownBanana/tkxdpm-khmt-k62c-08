package api.bank;

import com.ebike.bean.bank.BankAccount;
import com.ebike.bean.bank.BankTranfer;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class BankApi implements IBankApi {

    private static BankApi bankApi=new BankApi();
    public static final String PATH = "http://localhost:3000/";

    private Client client;
    private BankApi() {
    	client = ClientBuilder.newClient();
    }
    public static BankApi getInstance(){
        return bankApi;
    }

    @Override
    public boolean transfer(BankAccount sender, double payment) {
        try {
            BankTranfer tranfer = new BankTranfer();
            tranfer.setAccA(sender);
            tranfer.setMoneyTransfer(payment);
            WebTarget webTarget = client.target(PATH).path("transferAtoB");
            Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
            Response response = invocationBuilder.post(Entity.entity(tranfer, MediaType.APPLICATION_JSON));
            if ("ok".equals(response.readEntity(String.class)));
        }catch (Exception ex){
        }
        return true;
    }
    @Override
    public boolean transferBack(BankAccount sender, double payment) {
    	BankTranfer tranfer=new BankTranfer();
    	tranfer.setAccA(sender);
    	tranfer.setMoneyTransfer(payment);
    	WebTarget webTarget = client.target(PATH).path("transferBtoA");
    	Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(tranfer,MediaType.APPLICATION_JSON));
		if("ok".equals(response.readEntity(String.class)))
        return true;
		return false;
    }
    @Override
    public double getBalance(BankAccount bankAccount) {
        return 0;
    }
}
