var express = require('express');
var app = express();
var fs = require('fs');
var bodyParser = require('body-parser')
var rawdata = fs.readFileSync('data.json');


const SystemAccoutIndex = 0;

// create application/json parser
var jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })
app.use(bodyParser.json())

var allAcount = JSON.parse(rawdata);
console.log(allAcount[0]);

app.post('/transferAtoB', urlencodedParser, function (request, res) {
    let acc1, acc2;
    var check = false;
    console.log(request.body);
    allAcount.forEach((ac, index) => {

        if (request.body.accA.cardHolderName.toLowerCase() == ac.cardHolderName.toLowerCase() &&
            request.body.accA.cardNumber == ac.cardNumber &&
            request.body.accA.issuingBank.toLowerCase() == ac.issuingBank.toLowerCase() &&
            request.body.accA.expirationDate == ac.expirationDate &&
            request.body.accA.securityCode == ac.securityCode
        ) {

            check = true;

            acc1 = index;
        }
    });

    if (!check) {
        return res.send('Account not right');

    }
    check = false;
    if (parseInt(request.body.moneyTransfer, 10) < 0) {
        return res.send('fail');

    }
    if (allAcount[acc1].balance < request.body.moneyTransfer) {
        return res.send('transacion fail');

    }

    allAcount[acc1].balance -= request.body.moneyTransfer;
    allAcount[SystemAccoutIndex].balance += request.body.moneyTransfer;
    console.log(allAcount);
    var data = JSON.stringify(allAcount);
    fs.writeFileSync('data.json', data);
    return res.send('ok');
});
app.post('/transferBtoA', urlencodedParser, function (request, res) {
    let acc1, acc2;
    var check = false;
    console.log(request.body);
    console.log("ok");
    allAcount.forEach((ac, index) => {

        if (request.body.accA.cardHolderName.toLowerCase() == ac.cardHolderName.toLowerCase() &&
            request.body.accA.cardNumber == ac.cardNumber &&
            request.body.accA.issuingBank.toLowerCase() == ac.issuingBank.toLowerCase() &&
            request.body.accA.expirationDate == ac.expirationDate &&
            request.body.accA.securityCode == ac.securityCode
        ) {

            check = true;

            acc1 = index;
        }
    });

    if (!check) {
        console.log("ok1")
        return res.send('Account not right');

    }
    check = false;
    if (parseInt(request.body.moneyTransfer, 10) < 0) {
        console.log("ok")
        return res.send('fail');

    }
    allAcount[acc1].balance += request.body.moneyTransfer;
    allAcount[SystemAccoutIndex].balance -= request.body.moneyTransfer;
    console.log(allAcount);
    var data = JSON.stringify(allAcount);
    fs.writeFileSync('data.json', data);
    return res.send('ok');
});

app.post('/apitest', urlencodedParser, function (request, res) {
    let acc1, acc2;
    var check = false;
    console.log(request.body);
    console.log("ok");
    allAcount.forEach((ac, index) => {

        if (request.body.accA.cardHolderName.toLowerCase() == ac.cardHolderName.toLowerCase() &&
            request.body.accA.cardNumber == ac.cardNumber &&
            request.body.accA.issuingBank.toLowerCase() == ac.issuingBank.toLowerCase() &&
            request.body.accA.expirationDate == ac.expirationDate &&
            request.body.accA.securityCode == ac.securityCode
        ) {

            check = true;

            acc1 = index;
        }
    });

    if (!check) {
        console.log("ok1")
        return res.send('Account not right');

    }
    check = false;
    if (parseInt(request.body.moneyTransfer, 10) < 0) {
        console.log("ok")
        return res.send('fail');

    }
    if (allAcount[acc1].balance < request.body.moneyTransfer) {
        return res.send('transacion fail');

    }
    console.log(allAcount);
    var data = JSON.stringify(allAcount);
    fs.writeFileSync('data.json', data);
    return res.send('ok');
});

var server = app.listen(3000, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("Ung dung Node.js dang hoat dong tai dia chi: http://%s:%s", host, port)
});