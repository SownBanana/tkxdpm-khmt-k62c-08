# Bank API 


### How to use

* Install nodejs 
* cd to BankAPI folder
* npm run start
* Port: 3000
* Datafile: data.json

### Example: send POST request to localhost:3000/transferAtoB
Body:

        {"accA":{
        "cardholder_name":"CHU VIET DUNG",
        "card_number":19024705183012,
        "issuing_bank":"Techcombank",
        "expiration_date":"12/24",
        "security_code":"500"
        },
        "accB": {
        "cardholder_name":"Le NGOC DONG",
        "card_number":19024705183013,
        "issuing_bank":"bidv",
        "expiration_date":"12/24",
        "security_code":"100"
        },
        "moneyTransfer":10000
        }


