package com.ebike.db;

import java.util.ArrayList;

import com.ebike.entities.Bike;
import com.ebike.entities.Station;

public interface IStationDatabase extends IMediaDatabase<Station>{
	public Bike changeStation(Bike bike, Station desStation);
	public Bike rentBike(Station station, Bike bike);
	public Bike returnBike(Station station, Bike bike);
}
