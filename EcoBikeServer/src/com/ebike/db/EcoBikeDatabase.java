package com.ebike.db;

import java.util.ArrayList;
import java.util.Date;

import com.ebike.db.seed.Seed;
import com.ebike.entities.Bike;
import com.ebike.entities.EBike;
import com.ebike.entities.Order;
import com.ebike.entities.RegularBike;
import com.ebike.entities.RentingBike;
import com.ebike.entities.Station;
import com.ebike.entities.TwinBike;

public class EcoBikeDatabase implements IBikeDatabase<Bike>{

	private static IBikeDatabase<Bike> singleton = new EcoBikeDatabase();
	
//	private ArrayList<Bike> bikes = Seed.singleton().getBikes();
	private ArrayList<Station> stations = Seed.singleton().getStations();
	private ArrayList<Bike> rentingBikes = RentingBike.singleton().getRentingBikes();
	
	public static IBikeDatabase singleton() {
		return singleton;
	}
	
	@Override
	public ArrayList<Bike> search(Bike bike) {
		ArrayList<Bike> res = new ArrayList<Bike>();
		for (Station station : stations) {
			ArrayList<Bike> bikes = station.getBikes();
			for (Bike b: bikes) {
				if (b.match(bike)) {
					res.add(b);
				}
			}			
		}
		
		ArrayList<Bike> bikes = rentingBikes;
		for (Bike b: bikes) {
			if (b.match(bike)) {
				res.add(b);
			}
		}			
	
		return res;
	}

	@Override
	public Bike update(Bike bike) {
		for (Station station : stations) {
			ArrayList<Bike> bikes = new ArrayList<Bike>();
			bikes = station.getBikes();
			for (Bike b: bikes) {
				if (b.match(bike)) {
					bikes.remove(b);
					bikes.add(bike);
					return bike;
				}
			}
		}
		return null;
	}

	@Override
	public Bike add(Bike bike) {
		return null;
	}

	@Override
	public Bike delete(Bike t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double calPrice(Bike bike) {
//		for (Bike b : rentingBikes) {
//			if(b.match(bike)) {
////				int rentDuration = (int) Math.ceil((new Date().getTime() - b.getRentingTimeStart().getTime())/60000);
//				int rentDuration = 1;
//				return b.caculatePrice(rentDuration);
//			}
//		}
		return 0;
	}

	@Override
	public double calLongRentPrice(Bike bike) {
		//Not support yet
		return 0;
	}


	@Override
	public ArrayList<Bike> search(Station station, Bike bike) {
		if(station == null) {
			return search(bike);
		}
//		for (Station s : stations) {
//			if(s.match(station)) station = s;
//		}
		return station.searchBikes(bike);
	}

	@Override
	public Bike update(Station station, Bike bike) {
		for (Station s : stations) {
			if(s.match(station)) station = s;
		}
		Bike oldBike = new RegularBike();
		oldBike.setId(bike.getId());
		Bike b = station.searchBike(oldBike);
		if(b != null) {
			station.getBikes().remove(b);
			station.getBikes().add(bike);
			Seed.singleton().setStations(stations);		
			return b;
		}
		return null;
	}

	@Override
	public Bike add(Station station, Bike bike) {
		for (Station s : stations) {
			if(s.match(station)) station = s;
		}
		Bike oldBike = new RegularBike();
		oldBike.setId(bike.getId());
		Bike b = station.searchBike(oldBike);
		if(b != null) {
			
			return null;
		}
		station.getBikes().add(bike);
		Seed.singleton().setStations(stations);	
		return bike;
	}

	@Override
	public void delete(Station station, Bike bike) {
		for (Station s : stations) {
			if(s.match(station)) station = s;
		}
		Bike deleteBike = station.searchBike(bike);
		station.getBikes().remove(deleteBike);
		
	}
	
}
