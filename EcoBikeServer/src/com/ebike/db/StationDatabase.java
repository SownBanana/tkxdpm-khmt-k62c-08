package com.ebike.db;

import java.util.ArrayList;

import com.ebike.db.seed.Seed;
import com.ebike.entities.Bike;
import com.ebike.entities.Order;
import com.ebike.entities.RentingBike;
import com.ebike.entities.Station;

public  class StationDatabase implements IStationDatabase{

	private static IStationDatabase singleton = new StationDatabase();
	
	private ArrayList<Station> stations = Seed.singleton().getStations();
	public static IStationDatabase singleton() {
		return singleton;
	}
	@Override
	public ArrayList<Station> search(Station t) {
		ArrayList<Station> res = new ArrayList<Station>();
		System.out.println(stations);
		for (Station b: stations) {
			if (b.match(t)) {
				res.add(b);
			}
		}
		return res;
		
	}
	@Override
	public Station update(Station t) {
		for (Station b: stations) {
			if (b.equals(t)) {
				stations.remove(b);
				stations.add(t);
				Seed.singleton().setStations(stations);
				return t;
			}
		}
		return null;
	}
	@Override
	public Station add(Station t) {
		Integer index =1;
		for (Station station : stations) {
			if(Integer.valueOf(station.getId()) >= index) index =Integer.valueOf(station.getId()) + 1;
		}
		String stationId = index.toString();
		t.setId(stationId);
		if(t.getBikes() == null) t.setBikes(new ArrayList<Bike>());
		stations.add(t);
		Seed.singleton().setStations(stations);
		return t;
	}
	@Override
	public Station delete(Station t) {
		for (Station b: stations) {
			if (b.equals(t)) {
				stations.remove(b);
				Seed.singleton().setStations(stations);
				return b;
			}
		}
		return null;
	}
	@Override
	public Bike changeStation(Bike b, Station desStation) {
		for (Station station : stations) {
			Bike bike = station.searchBike(b);
			if(bike != null) {
				station.getBikes().remove(bike);
				desStation.getBikes().add(bike);
				Seed.singleton().setStations(stations);
				return bike;
			}
		}
		return null;
	}
	
	@Override
	public Bike rentBike(Station station, Bike bike) {
		if(station == null) {
			station = searchStationOfBike(bike);
		}
		Bike b = station.rentBike(bike);
		Seed.singleton().setStations(stations);
		return b;		
	}
	
	public Station searchStationOfBike(Bike bike) {
		for (Station station : stations) {
			ArrayList<Bike> bikes = station.getBikes();
			for (Bike b: bikes) {
				if (b.match(bike)) {
					return station;
				}
			}			
		}
		return null;
	}
	
	@Override
	public Bike returnBike(Station station, Bike bike) {
		Bike b = station.returnBike(bike);
		Seed.singleton().setStations(stations);
		return b;
	}

}
