package com.ebike.db;

import java.util.ArrayList;

import com.ebike.entities.Station;

public interface IBikeDatabase<T> extends IMediaDatabase<T>{
	public double calPrice(T t);
	public double calLongRentPrice(T t);
	public ArrayList<T> search(Station station, T t);
	public T update(Station station, T t);
	public T add(Station station, T t);
	public void delete(Station station, T t);
}
