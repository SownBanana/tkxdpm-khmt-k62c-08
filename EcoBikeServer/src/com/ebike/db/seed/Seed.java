package com.ebike.db.seed;

import com.ebike.entities.*;
import com.ebike.entities.price.Hours24Price;
import com.ebike.entities.price.NormalPrice;
import com.ebike.entities.price.Price;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class Seed {
	private ArrayList<Order> orders;
	private ArrayList<Bike> bikes;
	private ArrayList<Station> stations;
	private ArrayList<Price> prices;
	private static Seed singleton = new Seed();
		
	private Seed() {
		start();
	}
	
	public static Seed singleton() {
		return singleton;
	}
	
	private void start() {
		stations =new ArrayList<Station>();
		prices=new ArrayList<>();
		System.out.println(new File(getClass().getResource("./station.json").getPath()).toString());
		stations.addAll(generateStationFromFile(new File(getClass().getResource("./station.json").getPath()).toString()));
		prices.addAll(generatePriceFromFile2(new File(getClass().getResource("./price.json").getPath()).toString()));
		//generatePriceFromFile(new File(getClass().getResource("./price.json").getPath()).toString());
	}
	
	private void generatePriceFromFile(String filePath) {
		ObjectMapper mapper = new ObjectMapper();
		String json = FileHandler.read(filePath);
		System.out.println(json);
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			PriceManager rs = mapper.readValue(json, new TypeReference<PriceManager>() { });
			PriceManager.singleton().setRegularBikes(rs.getRegularBikes());
			PriceManager.singleton().seteBikes(rs.geteBikes());
			PriceManager.singleton().setTwinBikes(rs.getTwinBikes());
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from ");
		}
	}

	private void savePriceToFile(String filePath) {
		ObjectMapper mapper = new ObjectMapper();
		String json = "";
		try {
			json = mapper.writeValueAsString(PriceManager.singleton());
			System.out.println(json);
			FileHandler.write(filePath, json);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private ArrayList<Station> generateStationFromFile(String filePath){
		ArrayList<Station> res = new ArrayList<Station>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileHandler.read(filePath);
		System.out.println(json);
		try {
			
			res = mapper.readValue(json, new TypeReference<ArrayList<Station>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		
		return res;
	}
	
	private ArrayList<Station> generateStationFromFile2(String filePath){
		ArrayList<Station> res = new ArrayList<Station>();
		ObjectMapper mapper = new ObjectMapper();
		Gson gson=new Gson();
		
		String json = FileHandler.read(filePath);
		System.out.println(json);
		try {

			JSONArray jsonArray= new JSONArray(json);
			for(int i=0;i<jsonArray.length();i++){
				JSONObject station=jsonArray.getJSONObject(i);
				JSONArray bikes=station.getJSONArray("bikes");
				ArrayList<Bike> bikeArrayList=new ArrayList<>();
				for(int j=0;j<bikes.length();j++){
					JSONObject bike=bikes.getJSONObject(j);
					System.out.printf(bike.getString("class"));
					if(bike.getString("class").equals("eBike")){
						EBike ebike = gson.fromJson(bike.toString(),EBike.class);
						bikeArrayList.add(ebike);
					}
					if(bike.getString("class").equals("twinBike")){
						TwinBike ebike = gson.fromJson(bike.toString(),TwinBike.class);
						bikeArrayList.add(ebike);
					}
					if(bike.getString("class").equals("regularBike")){
						RegularBike ebike = gson.fromJson(bike.toString(),RegularBike.class);
						bikeArrayList.add(ebike);
					}
				}
				Station station1=gson.fromJson(station.toString(),Station.class);
				station1.setBikes(bikeArrayList);
				res.add(station1);
			}
			//res = mapper.readValue(json, new TypeReference<ArrayList<Station>>() { });
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		
		return res;
	}

	private ArrayList<Price> generatePriceFromFile2(String filePath){
		ArrayList<Price> res = new ArrayList<Price>();
		ObjectMapper mapper = new ObjectMapper();
		Gson gson=new Gson();

		String json = FileHandler.read(filePath);
		System.out.println(json);
		try {

			JSONArray jsonArray= new JSONArray(json);
			for(int i=0;i<jsonArray.length();i++){
				JSONObject price=jsonArray.getJSONObject(i);
				if(price.getString("type").equals("normalPrice")){
					NormalPrice normalPrice = gson.fromJson(price.toString(),NormalPrice.class);
					res.add(normalPrice);
				}
				else if(price.getString("type").equals("hourPrice")){
					Hours24Price normalPrice = gson.fromJson(price.toString(),Hours24Price.class);
					res.add(normalPrice);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		return res;
	}
	
	
	private void saveStationToFile(String filePath, ArrayList<Station> stations) {
		ObjectMapper mapper = new ObjectMapper();
		
		ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
		try {
			writer.writeValue(new File(filePath), stations);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		ObjectMapper mapper = new ObjectMapper();
//		String json;
//		try {
//			json = mapper.writerFor(new TypeReference<List<Station>>() {}).writeValueAsString(stations);
//			System.out.println(json);
//			FileHandler.write(filePath, json);
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	
	
	private ArrayList<Bike> generateBikeFromFile(String filePath){
		ArrayList<Bike> res = new ArrayList<Bike>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileHandler.read(filePath);
		System.out.println(json);
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<Bike>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		
		return res;
	}
	
	private void saveBikeToFile(String filePath, ArrayList<Bike> bikes) {
		ObjectMapper mapper = new ObjectMapper();
		String json;
		try {
			json = mapper.writerFor(new TypeReference<List<Bike>>() {}).writeValueAsString(bikes);
			FileHandler.write(filePath, json);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public ArrayList<Order> getOrders() {
		return orders;
	}
	
	public ArrayList<Bike> getBikes() {
		return bikes;
	}

	public ArrayList<Price> getPrices() {
		return prices;
	}

	public void setPrices(ArrayList<Price> prices) {
		this.prices = prices;
	}

	public ArrayList<Station> getStations() {
		
		return stations;
	}
	public void setBikes(ArrayList<Bike> bikes) {
		this.bikes = bikes;
		saveBikeToFile(new File(getClass().getResource("./ecobike.json").getPath()).toString(), bikes);
	}
	
	public void setStations(ArrayList<Station> stations) {
		this.stations = stations;
		saveStationToFile(new File(getClass().getResource("./station.json").getPath()).toString(), stations);
	}
	

	public static void main(String[] args) {
		
//		Seed.singleton().generatePriceFromFile("P:\\Code4Life\\TK&XDPM\\tkxdpm-khmt-k62c-08\\EcoBikeServer\\bin\\com\\ebike\\db\\seed\\price.json");
//		System.out.println(PriceManager.singleton());
		
//		Price hour24hBike=new Hours24Price(500000,200000,1440,720,10000,60,2000);
//		Price hour24hRegular=new Hours24Price(700000,200000,1440,720,10000,60,2000);
//		Price hour24hTwin=new Hours24Price(800000,200000,1440,720,10000,60,2000);
//
//		Price noramlBike=new NormalPrice(500000,10000,30,3000,15);
//		Price noramlRegular=new NormalPrice(700000,10000,30,3000,15);
//		Price noramlTwin=new NormalPrice(800000,10000,30,3000,15);
//		PriceManager.singleton()regularBikePrices.add(hour24hRegular);
//		regularBikePrices.add(noramlRegular);
//		twinBikePrices.add(noramlTwin);
//		twinBikePrices.add(hour24hTwin);
//		eBikePrices.add(noramlBike);
//		eBikePrices.add(hour24hBike);
		
//		Date date1 = new Date();
//		System.out.println(date1.getTime());
//		System.out.println(date1.toString());
//		try {
//			Thread.sleep(3000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		Date date2 = new Date();
//		System.out.println(date2.getTime());
//		System.out.println(date2.toString());
//		
//		long diffInMillies = date2.getTime() - date1.getTime();
//		System.out.println(diffInMillies);
//		TimeUnit timeUnit = TimeUnit.MINUTES;
//		System.out.println(timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS));
		
//		Seed seed = new Seed();
//		System.out.println(seed.stations.toString());
//		seed.saveStationToFile("D://testttt.json", seed.stations);
		
//		Station station = new Station("01", "MK", "TN-MK");
//		ArrayList<Bike> bikes = new ArrayList<Bike>();
//		RegularBike bike1 = new RegularBike("12", "4324", "4324324", "434", "4324");
//		RegularBike bike2 = new RegularBike("123", "4324", "4324324", "434", "4324");
//		TwinBike bike3 = new TwinBike("1234", "4324", "4324324", "434", "4324");
//		EBike bike4 = new EBike("1245", "4324", "4324324", "434", "4324");
//		EBike bike5 = new EBike("12546", "4324", "4324324", "434", "4324");
//		TwinBike bike6 = new TwinBike("12765", "4324", "4324324", "434", "4324");
//		bikes.add(bike1);
//		bikes.add(bike2);
//		bikes.add(bike3);
//		bikes.add(bike4);
//		bikes.add(bike5);
//		bikes.add(bike6);
//		station.setBikes(bikes);
//		System.out.println(bikes.toString());
//		System.out.println(station.getBikes().toString());
//		ArrayList<Station> stations = new ArrayList<Station>();
//		stations.add(station);
//		seed.saveStationToFile("D:\\test.json", stations);
		
//		seed.start();
//		ArrayList<Station> stations = seed.generateStationFromFile("P:\\Code4Life\\TK&XDPM\\tkxdpm-khmt-k62c-08\\EcoBikeServer\\src\\com\\ebike\\db\\seed\\station.json");
//		System.out.println(stations);
//		ObjectMapper mapper = new ObjectMapper();
//		
//		ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
//		try {
//			writer.writeValue(new File("P:\\Code4Life\\TK&XDPM\\tkxdpm-khmt-k62c-08\\EcoBikeServer\\src\\com\\ebike\\db\\seed\\tesst.json"), stations);
//		} catch (JsonGenerationException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (JsonMappingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		
//		ObjectMapper mapper = new ObjectMapper();
//		
//		Price nprice = new NormalPrice(10000, 30, 3000, 15);
//		Price hPrice = new Hours24Price(20000, 1440, 720, 10000, 60, 2000, 15);
//		List<Price> price = new ArrayList<Price>();
//		price.add(nprice);
//		price.add(hPrice);
//		PriceManager.singleton().setRegularBikes(price);
//		PriceManager.singleton().seteBikes(price);
//		PriceManager.singleton().setTwinBikes(price);
//		
//		Seed.singleton().savePriceToFile("D:\\testPrice.json");
		
//		mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
//		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
//		String json = "";
//		try {
//			json = mapper.writeValueAsString(PriceManager.singleton());
//			System.out.println(json);
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		JsonNode jsonNode = objectMapper.readTree(json);
//		String color = jsonNode.get("color").asText();
		
//		
//		String json = FileHandler.read("P:\\Code4Life\\TK&XDPM\\tkxdpm-khmt-k62c-08\\EcoBikeServer\\src\\com\\ebike\\db\\seed\\price.json");
//		System.out.println(json);
//		try {
//			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
//			PriceManager rs = mapper.readValue(json, new TypeReference<PriceManager>() { });
//			PriceManager.singleton().setRegularBike(rs.getRegularBike());
//			PriceManager.singleton().seteBike(rs.geteBike());
//			PriceManager.singleton().setTwinBike(rs.getTwinBike());
//		} catch (IOException e) {
//			e.printStackTrace();
//			System.out.println("Invalid JSON input data from ");
//		}
//		
//		System.out.println(PriceManager.singleton());
		
		
//		Seed seed = new Seed();
//		seed.start();
//		BikePrice bikePrice = new BikePrice(30000, 30, 3000, 2, 200000, 24, 12, 2, 30, 10, 12);
//		RegularBike bike = new RegularBike("1", "Ten", "Mo ta", "Nhan hieu", "BKS", 2, bikePrice);
//		Bike tbike = new TwinBike("1", "Ten", "Mo ta", "Nhan hieu", "BKS", 2, bikePrice);
//		ArrayList<RegularBike> bikes = new ArrayList<RegularBike>();
//		bikes.add(bike);
//		bikes.add(tbike);
		
//		Seed.singleton().setBikes((ArrayList<Bike>)bikes);
		
//		ObjectMapper mapper = new ObjectMapper();
		
//		seed.saveBikeToFile(seed.PATH, bikes);

//		seed.getPath();
//		BikePrice bikePrice = new BikePrice(30000, 30, 3000, 2, 200000, 24, 12, 2, 30, 10, 12);
//		RegularBike bike = new RegularBike("1", "Ten", "Mo ta", "Nhan hieu", "BKS", 2, bikePrice);
//		TwinBike tbike = new TwinBike("1", "Ten", "Mo ta", "Nhan hieu", "BKS", 2, bikePrice);
//		ArrayList<Bike> bikes = new ArrayList<Bike>();
//		bikes.add(bike);
//		bikes.add(tbike);
//		seed.saveBikeToFile(seed.PATH, bikes);

//        String json;
//		try {
//			json = mapper.writerFor(new TypeReference<List<Bike>>() {}).writeValueAsString(bikes);
//			FileHandler.write("P:\\Code4Life\\TK&XDPM\\tkxdpm-khmt-k62c-08\\EcoBikeServer\\src\\com\\ebike\\db\\seed\\tesst.json", json);
//			System.out.println(json);
//			ArrayList<Bike> listBikes = mapper.readValue(json, new TypeReference<ArrayList<Bike>>() { });
//			
//			for (Bike b : listBikes) {
//				System.out.println(b instanceof RegularBike);
//			}
////			System.out.println(mapper.readValue(json, new TypeReference<ArrayList<Bike>>() { }).toString());
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
}
