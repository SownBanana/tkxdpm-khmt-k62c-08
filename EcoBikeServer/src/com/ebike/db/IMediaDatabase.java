package com.ebike.db;

import java.util.ArrayList;

import com.ebike.entities.Bike;
import com.ebike.entities.Order;
import com.ebike.entities.Station;

public interface IMediaDatabase<T> {
	public ArrayList<T> search(T t);
	public T update(T t);
	public T add(T t);
	public T  delete(T t);
	
}

