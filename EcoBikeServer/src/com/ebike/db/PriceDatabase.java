package com.ebike.db;

import com.ebike.db.seed.Seed;
import com.ebike.entities.Station;
import com.ebike.entities.price.Price;

import java.util.ArrayList;

public class PriceDatabase implements IMediaDatabase<Price> {
    private static IMediaDatabase singleton = new PriceDatabase();

    private ArrayList<Price> prices = Seed.singleton().getPrices();
    public static IMediaDatabase singleton() {
        return singleton;
    }
    @Override
    public ArrayList<Price> search(Price price) {
        return prices;
    }

    @Override
    public Price update(Price price) {
        return null;
    }

    @Override
    public Price add(Price price) {
        return null;
    }

    @Override
    public Price delete(Price price) {
        return null;
    }
}
