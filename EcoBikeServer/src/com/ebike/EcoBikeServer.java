package com.ebike;

import com.ebike.entities.EBike;
import com.ebike.entities.TwinBike;
import com.ebike.service.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class EcoBikeServer {
	public static final int PORT = 8080;
	
	public static void main(String[] args) throws Exception {
		
		
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");

		Server jettyServer = new Server(PORT);
		jettyServer.setHandler(context);

		ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
		jerseyServlet.setInitOrder(0);

		
		jerseyServlet.setInitParameter("jersey.config.server.provider.classnames",
				BikeService.class.getCanonicalName()  + ", " + 
				StationService.class.getCanonicalName()		+", "+ EBikeService.class.getCanonicalName()
				+", "+ TwinBikeService.class.getCanonicalName()+", "+ RegularBikeService.class.getCanonicalName()
				+", "+ PriceService.class.getCanonicalName()
		);

		
		  try {
		      jettyServer.start();
		      jettyServer.join();
		    } catch (Exception e){
		       System.out.println(e);
		        jettyServer.stop();
		        jettyServer.destroy();
		    }
	}
}