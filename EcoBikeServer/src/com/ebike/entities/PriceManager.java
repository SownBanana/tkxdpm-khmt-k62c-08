package com.ebike.entities;

import java.util.List;

import com.ebike.db.IMediaDatabase;
import com.ebike.entities.price.Price;

public class PriceManager {
	private List<Price> regularBikes;
	private List<Price> eBikes;
	private List<Price> twinBikes;
	private static PriceManager singleton = new PriceManager();
	
	public static PriceManager singleton() {
		return singleton;
	}
	
	

	public List<Price> getRegularBikes() {
		return regularBikes;
	}



	public void setRegularBikes(List<Price> regularBikes) {
		this.regularBikes = regularBikes;
	}



	public List<Price> geteBikes() {
		return eBikes;
	}



	public void seteBikes(List<Price> eBikes) {
		this.eBikes = eBikes;
	}



	public List<Price> getTwinBikes() {
		return twinBikes;
	}



	public void setTwinBikes(List<Price> twinBikes) {
		this.twinBikes = twinBikes;
	}


}
