package com.ebike.entities;

public class BikeEntity extends Bike {
    private String type;
    BikeEntity(){
    }

    public BikeEntity(String type) {
        this.type = type;
    }

    public BikeEntity(String id, String name, String description, String brand, String licence, int booking, String type) {
        super(id, name, description, brand, licence, booking);
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
