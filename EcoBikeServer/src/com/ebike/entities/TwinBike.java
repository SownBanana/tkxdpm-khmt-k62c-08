package com.ebike.entities;

import java.util.List;

import com.ebike.entities.price.Price;

public class TwinBike extends Bike{
//	private static List<Price> prices;
	
	public TwinBike() {
		super();
		// TODO Auto-generated constructor stub
	}


	public TwinBike(String id, String name, String description, String brand, String licence, int booking
//			,List<Price> price
			) {
		super(id, name, description, brand, licence, booking);
//		this.prices = prices;
		// TODO Auto-generated constructor stub
	}

	public TwinBike(String id, String name, String description, String brand, String licence) {
		super(id, name, description, brand, licence);
		// TODO Auto-generated constructor stub
	}

	
	@Override
	public boolean match(Bike bike) {
		// TODO Auto-generated method stub
		if(!(bike instanceof TwinBike) && (bike instanceof RegularBike || bike instanceof EBike)) return false;
		return super.match(bike);
	}

//	@Override
//	public double caculatePrice(long minutes) {
//		return 0;
//	}
	
}
