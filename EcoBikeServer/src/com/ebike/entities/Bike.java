package com.ebike.entities;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "class")
@JsonSubTypes({ 
	@JsonSubTypes.Type(value = RegularBike.class, name = "regularBike"), 
	@JsonSubTypes.Type(value = TwinBike.class, name = "twinBike"), 
	@JsonSubTypes.Type(value = EBike.class, name = "eBike")
})


public class Bike{
	private String id;
	private String name;
	private String description;
	private String brand;
	private String licence;
	private int booking;

	
	public Bike() {
		super();
	}
	
	public Bike(String id, String name, String description, String brand, String licence) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.brand = brand;
		this.licence = licence;
		this.booking = 0;
	}

	public Bike(String id, String name, String description, String brand, String licence, int booking
			) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.brand = brand;
		this.licence = licence;
		this.booking = booking;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getLicence() {
		return licence;
	}

	public void setLicence(String licence) {
		this.licence = licence;
	}

	public int getBooking() {
		return booking;
	}

	public void setBooking(int booking) {
		this.booking = booking;
	}
	

//	public Date getRentingTimeStart() {
//		return rentingTimeStart;
//	}
//
//	public void setRentingTimeStart(Date rentingTimeStart) {
//		this.rentingTimeStart = rentingTimeStart;
//	}
//
//	public Date getRentingTimeEnd() {
//		return rentingTimeEnd;
//	}
//
//	public void setRentingTimeEnd(Date rentingTimeEnd) {
//		this.rentingTimeEnd = rentingTimeEnd;
//	}

	public boolean match(Bike bike) {
		if (bike == null)
			return true;
		if (bike.id != null && !bike.id.equals("") && !this.id.toLowerCase().equals(bike.id.toLowerCase())) {
			return false;
		}

		if (bike.name != null && !bike.name.equals("") && !this.name.toLowerCase().contains(bike.name.toLowerCase())) {
			return false;
		}
		if (bike.description != null && !bike.description.equals("") && !this.description.toLowerCase().contains(bike.description.toLowerCase())) {
			return false;
		}
		if (bike.brand != null && !bike.brand.equals("") && !this.brand.toLowerCase().contains(bike.brand.toLowerCase())) {
			return false;
		}
		if (bike.licence != null && !bike.licence.equals("") && !this.licence.toLowerCase().contains(bike.licence.toLowerCase())) {
			return false;
		}
		return true;
	}
	
	public boolean matchAsBike(Bike bike) {
		return this.match(bike);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Bike) {
			return this.id.equals(((Bike) obj).id);
		}
		return false;
	}

	@Override
	public String toString() {
		return "Bike [id=" + id + ", name=" + name + ", description=" + description + ", brand=" + brand + ", licence="
				+ licence + ", booking=" + booking 
//				+ ", price=" + price 
				+ "]";
	}

//	@Override
//	public double caculatePrice(long minutes) {
//		// TODO Auto-generated method stub
//		return 0;
//	}
	
	
}
