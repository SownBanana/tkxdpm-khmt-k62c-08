package com.ebike.entities;

import java.util.ArrayList;
import java.util.Date;

public class RentingBike {
	private ArrayList<Bike> rentingBikes;
	private static RentingBike singleton = new RentingBike();
	public RentingBike() {
		super();
		rentingBikes = new ArrayList<Bike>();
	}
	
	public static RentingBike singleton() {
		return singleton;
	}
	
	
	public ArrayList<Bike> getRentingBikes() {
		return rentingBikes;
	}

	public void setRentingBikes(ArrayList<Bike> rentingBikes) {
		this.rentingBikes = rentingBikes;
	}

	public void rentBike(Bike bike) {
//		bike.setRentingTimeStart(new Date());
		this.rentingBikes.add(bike);
	}
	public Bike returnBike(Bike bike) {
		for (Bike b : rentingBikes) {
			if(b.match(bike)) {
				rentingBikes.remove(b);
//				b.setRentingTimeEnd(new Date());
				return b;
			}
		}
		return null;
	}
	
}
