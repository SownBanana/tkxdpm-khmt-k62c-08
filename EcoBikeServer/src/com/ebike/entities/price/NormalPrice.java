package com.ebike.entities.price;

import java.util.Date;

public class NormalPrice extends Price{
	
	private double firstPrice;
	private int firstDuration;
	private double stepPrice;
	private int stepDuration;
	
	

    public NormalPrice() {
		super();
	}

	public NormalPrice(double firstPrice, int firstDuration, double stepPrice, int stepDuration) {
        this.firstPrice = firstPrice;
        this.firstDuration = firstDuration;
        this.stepPrice = stepPrice;
        this.stepDuration = stepDuration;
    }

    public double caculatePrice(long minutes) {
		double rs=0;
		if(minutes <= firstDuration) rs = firstPrice;
		else {
			rs = firstPrice + stepPrice*Math.ceil(minutes/stepDuration);
		}
		return rs;
	}

    public double getFirstPrice() {
        return firstPrice;
    }

    public void setFirstPrice(double firstPrice) {
        this.firstPrice = firstPrice;
    }

    public int getFirstDuration() {
        return firstDuration;
    }

    public void setFirstDuration(int firstDuration) {
        this.firstDuration = firstDuration;
    }

    public double getStepPrice() {
        return stepPrice;
    }

    public void setStepPrice(double stepPrice) {
        this.stepPrice = stepPrice;
    }

    public int getStepDuration() {
        return stepDuration;
    }

    public void setStepDuration(int stepDuration) {
        this.stepDuration = stepDuration;
    }

	@Override
	public double caculatePrice(Date startDate, Date finishDate) {
		// TODO Auto-generated method stub
		return 0;
	}
}
