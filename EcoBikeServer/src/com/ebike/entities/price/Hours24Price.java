package com.ebike.entities.price;

import java.util.Date;

public class Hours24Price extends Price{
	
	private double longRentPrice;
	private int longRentDuration;
	private int refundPivot;
	private double refundStepPrice;
	private int refundStepDuration;
	private double overtimeStepPrice;
	private int overtimeStepDuration;

	
	
    public Hours24Price() {
		super();
	}

	public Hours24Price(double longRentPrice, int longRentDuration, int refundPivot, double refundStepPrice, int refundStepDuration,
                        double overtimeStepPrice, int overtimeStepDuration) {
        this.longRentPrice = longRentPrice;
        this.longRentDuration = longRentDuration;
        this.refundPivot = refundPivot;
        this.refundStepPrice = refundStepPrice;
        this.refundStepDuration = refundStepDuration;
        this.overtimeStepPrice = overtimeStepPrice;
        this.overtimeStepDuration = overtimeStepDuration;
    }

    public double caculatePrice(long minutes) {
    	
		double rs = 0;
		if(minutes >= (refundPivot - refundStepDuration) && minutes <= longRentDuration) rs = longRentPrice;
		else if(minutes < (refundPivot - refundStepDuration)) {
			rs = longRentPrice - refundStepPrice*Math.ceil((refundPivot - minutes)/refundStepDuration);
		}else {
			rs = longRentPrice + overtimeStepPrice*Math.ceil((minutes - longRentDuration)/overtimeStepDuration);
		}
		return rs;
	}

    public double getLongRentPrice() {
        return longRentPrice;
    }

    public void setLongRentPrice(double longRentPrice) {
        this.longRentPrice = longRentPrice;
    }

    public int getLongRentDuration() {
        return longRentDuration;
    }

    public void setLongRentDuration(int longRentDuration) {
        this.longRentDuration = longRentDuration;
    }

    public int getRefundPivot() {
        return refundPivot;
    }

    public void setRefundPivot(int refundPivot) {
        this.refundPivot = refundPivot;
    }

    public double getRefundStepPrice() {
        return refundStepPrice;
    }

    public void setRefundStepPrice(double refundStepPrice) {
        this.refundStepPrice = refundStepPrice;
    }

    public int getRefundStepDuration() {
        return refundStepDuration;
    }

    public void setRefundStepDuration(int refundStepDuration) {
        this.refundStepDuration = refundStepDuration;
    }

    public double getOvertimeStepPrice() {
        return overtimeStepPrice;
    }

    public void setOvertimeStepPrice(double overtimeStepPrice) {
        this.overtimeStepPrice = overtimeStepPrice;
    }

    public int getOvertimeStepDuration() {
        return overtimeStepDuration;
    }

    public void setOvertimeStepDuration(int overtimeStepDuration) {
        this.overtimeStepDuration = overtimeStepDuration;
    }

	@Override
	public double caculatePrice(Date startDate, Date finishDate) {
		// TODO Auto-generated method stub
		return 0;
	}
}
