package com.ebike.entities;

import java.util.List;

import com.ebike.entities.price.Price;

public class EBike extends Bike{
//	private static List<Price> prices;

	public EBike() {
		super();
		// TODO Auto-generated constructor stub
	}


	public EBike(String id, String name, String description, String brand, String licence, int booking) {
		super(id, name, description, brand, licence, booking);
//		this.prices = price;
		// TODO Auto-generated constructor stub
	}

	public EBike(String id, String name, String description, String brand, String licence) {
		super(id, name, description, brand, licence);
		// TODO Auto-generated constructor stub
	}


	@Override
	public boolean match(Bike bike) {
		// TODO Auto-generated method stub
		if(!(bike instanceof EBike) && (bike instanceof RegularBike || bike instanceof TwinBike)) return false;
		return super.match(bike);
		
	}

//	@Override
//	public double caculatePrice(long minutes) {
////		return EBike.getPrices().get(0).caculatePrice(minutes);
//		return 0;
//	}
//	
}
