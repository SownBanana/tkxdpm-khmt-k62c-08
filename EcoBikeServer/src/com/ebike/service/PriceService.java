package com.ebike.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ebike.db.*;
import com.ebike.entities.Bike;
import com.ebike.entities.price.Hours24Price;
import com.ebike.entities.price.NormalPrice;
import com.ebike.entities.price.Price;


@Path("/price")
public class PriceService {
    private IMediaDatabase<Price> priceDatabase;

    public PriceService() {
        priceDatabase= PriceDatabase.singleton();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Price> getPrices() {
       return priceDatabase.search(null);
    }

    @GET()
    @Path("/hour-prices")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Hours24Price> getHourPrices() {
        ArrayList<Price> prices=priceDatabase.search(null);
        ArrayList<Hours24Price> hours24Prices=new ArrayList<>();
        for (Price price :prices
        ) {
            if(price instanceof Hours24Price)
                hours24Prices.add((Hours24Price) price);
        }
        return hours24Prices;
    }

    @GET()
    @Path("/hour-price")
    @Produces(MediaType.APPLICATION_JSON)
    public Hours24Price getHourPrice(@QueryParam("bike-type") String bikeType) {
        ArrayList<Price> prices=priceDatabase.search(null);
        ArrayList<Hours24Price> hours24Prices=new ArrayList<>();
        for (Price price :prices
        ) {
            if(price instanceof Hours24Price)
                hours24Prices.add((Hours24Price) price);
        }
        if(bikeType!=null){
            for (Hours24Price price: hours24Prices
            ) {
                if(price.getBikeType().equals(bikeType)){
                    return price;
                }
            }
        }
        return null;
    }

    @GET
    @Path("/normal-prices")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<NormalPrice> getNormalPrices( ) {
        ArrayList<Price> prices=priceDatabase.search(null);
        ArrayList<NormalPrice> normalPrices=new ArrayList<>();
        for (Price price :prices
        ) {
            if(price instanceof NormalPrice)
                normalPrices.add((NormalPrice) price);
        }
        return normalPrices;

    }

    @GET
    @Path("/normal-price")
    @Produces(MediaType.APPLICATION_JSON)
    public NormalPrice getNormalPrice(@QueryParam("bike-type") String bikeType ) {
        ArrayList<Price> prices=priceDatabase.search(null);
        ArrayList<NormalPrice> normalPrices=new ArrayList<>();
        for (Price price :prices
        ) {
            if(price instanceof NormalPrice)
                normalPrices.add((NormalPrice) price);
        }
        if(bikeType!=null){
            for (NormalPrice price: normalPrices
            ) {
                if(price.getBikeType().equals(bikeType)){
                    return price;
                }
            }
        }
        return null;

    }



    @POST
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Bike updateBike(@QueryParam("stationid") String stationid, @PathParam("id") String id, Bike bike) {
       return null;
    }
}
