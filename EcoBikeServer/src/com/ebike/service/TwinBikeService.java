package com.ebike.service;

import com.ebike.db.EcoBikeDatabase;
import com.ebike.db.IBikeDatabase;
import com.ebike.db.IMediaDatabase;
import com.ebike.db.StationDatabase;
import com.ebike.entities.Bike;
import com.ebike.entities.EBike;
import com.ebike.entities.Station;
import com.ebike.entities.TwinBike;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

@Path("/twin-bike")
public class TwinBikeService {
    private IBikeDatabase<Bike> bikeDatabase;
    private IMediaDatabase<Station> staionDatabase;

    public TwinBikeService() {
        bikeDatabase = EcoBikeDatabase.singleton();
        staionDatabase = StationDatabase.singleton();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<TwinBike> getBikes(@QueryParam("station-id") String stationid, @QueryParam("id") String id, @QueryParam("name") String name,
                                        @QueryParam("type") String type, @QueryParam("description") String description,
                                        @QueryParam("brand") String brand, @QueryParam("licence") String licence) {

        Station station = new Station();
        if(stationid != null) {
            station.setId(stationid);
            station = staionDatabase.search(station).get(0);
        }else {
            station = null;
        }
        System.out.println(station);
        ArrayList<Bike> res;
        Bike bike = new Bike(id, name, description, brand, licence);
        res = bikeDatabase.search(station, bike);
        ArrayList<TwinBike> bikes=new ArrayList<>();
        for (Bike bikeEntity:res
        ) {
            if(bikeEntity instanceof TwinBike)
                bikes.add((TwinBike) bikeEntity);
        }
        return bikes;
    }


   
}
