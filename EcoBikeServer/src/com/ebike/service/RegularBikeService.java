package com.ebike.service;

import com.ebike.db.EcoBikeDatabase;
import com.ebike.db.IBikeDatabase;
import com.ebike.db.IMediaDatabase;
import com.ebike.db.StationDatabase;
import com.ebike.entities.Bike;
import com.ebike.entities.EBike;
import com.ebike.entities.RegularBike;
import com.ebike.entities.Station;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

@Path("/regular-bike")
public class RegularBikeService {
    private IBikeDatabase<Bike> bikeDatabase;
    private IMediaDatabase<Station> staionDatabase;

    public RegularBikeService() {
        bikeDatabase = EcoBikeDatabase.singleton();
        staionDatabase = StationDatabase.singleton();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<RegularBike> getBikes(@QueryParam("station-id") String stationid, @QueryParam("id") String id, @QueryParam("name") String name,
                                           @QueryParam("type") String type, @QueryParam("description") String description,
                                           @QueryParam("brand") String brand, @QueryParam("licence") String licence) {

        Station station = new Station();
        if(stationid != null) {
            station.setId(stationid);
            station = staionDatabase.search(station).get(0);
        }else {
            station = null;
        }
        System.out.println(station);
        ArrayList<Bike> res;
        Bike bike = new Bike(id, name, description, brand, licence);
        res = bikeDatabase.search(station, bike);
        ArrayList<RegularBike> bikes=new ArrayList<>();
        for (Bike bikeEntity:res
        ) {
            if(bikeEntity instanceof RegularBike)
                bikes.add((RegularBike) bikeEntity);
        }
        return bikes;
    }


    
}
