package com.ebike.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ebike.db.EcoBikeDatabase;
import com.ebike.db.IBikeDatabase;
import com.ebike.db.IMediaDatabase;
import com.ebike.db.StationDatabase;
import com.ebike.entities.Bike;
import com.ebike.entities.EBike;
import com.ebike.entities.RegularBike;
import com.ebike.entities.Station;
import com.ebike.entities.TwinBike;


@Path("/bikes")
public class BikeService {
	private IBikeDatabase<Bike> bikeDatabase;
	private IMediaDatabase<Station> staionDatabase;

	public BikeService() {
		bikeDatabase = EcoBikeDatabase.singleton();
		staionDatabase = StationDatabase.singleton();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Bike> getBikes(@QueryParam("stationid") String stationid, @QueryParam("id") String id, @QueryParam("name") String name,
			@QueryParam("type") String type, @QueryParam("description") String description,
			@QueryParam("brand") String brand, @QueryParam("licence") String licence, @QueryParam("class") String bikeClass) {
		
		Station station = new Station();
		
		if(stationid != null) {
			station.setId(stationid);
			station = staionDatabase.search(station).get(0);			
		}else {
			station = null;
		}
		ArrayList<Bike> res;
		Bike bike = new Bike(id, name, description, brand, licence);
		if(bikeClass != null) {
			if(bikeClass.equals("eBike"))
				bike = new EBike(id, name, description, brand, licence);
			else if(bikeClass.equals("twinBike"))
				bike = new TwinBike(id, name, description, brand, licence);	
			else if(bikeClass.equals("regularBike")) {
				bike = new RegularBike(id, name, description, brand, licence);	
			}
		}
		res = bikeDatabase.search(station, bike);
		return res;

	}
	
	
	
	
	
	
	@POST
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Bike updateBike(@QueryParam("stationid") String stationid,Bike bike) {
		Station station = new Station();
		station.setId(stationid);
		return bikeDatabase.update(station,  bike);
	}
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Bike addBike(@QueryParam("stationid") String stationid,Bike bike) {
		Station station = new Station();
		station.setId(stationid);
		return bikeDatabase.add(station,  bike);
		
	}
	@POST
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Bike deleteBike(@QueryParam("stationid") String stationid,Bike bike) {
		Station station = new Station();
		station.setId(stationid);
		bikeDatabase.delete(station,  bike);
		return bike;
	}
	
	
}
