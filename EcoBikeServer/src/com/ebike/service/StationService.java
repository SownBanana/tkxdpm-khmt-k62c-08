package com.ebike.service;

import java.lang.reflect.Array;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ebike.db.EcoBikeDatabase;
import com.ebike.db.IMediaDatabase;
import com.ebike.db.IStationDatabase;
import com.ebike.db.StationDatabase;
import com.ebike.entities.Bike;
import com.ebike.entities.Station;

@Path("/stations")
public class StationService {
	private IStationDatabase stationDatabase;

	public StationService() {
		stationDatabase = StationDatabase.singleton();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Station> getStations(@QueryParam("id") String id, @QueryParam("name") String name,
			@QueryParam("address") String address) {
		Station station = new Station(id, name, address);
		ArrayList<Station> res = stationDatabase.search(station);
		return res;
	}

	@GET
	@Path("/freeDocks")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Station> getStationsHaveFreeDocks() {
		ArrayList<Station> resDb = stationDatabase.search(null);
		ArrayList<Station> res = new ArrayList<>();
		for (Station station: resDb
			 ) {
			if(station.getNumberOfEmptyDocks()>0)
				res.add(station);
		}
		return res;
	}

	@GET
	@Path("/change-station")
	@Produces(MediaType.APPLICATION_JSON)
	public Bike moveBikeTo(@QueryParam("bikeid") String bikeid, @QueryParam("des-stationid") String stationid) {
		Station desStation = stationDatabase.search(new Station(stationid, null, null)).get(0);
		Bike bike = new Bike(bikeid, null, null, null, null);
		return stationDatabase.changeStation(bike, desStation);
	}
	
	@GET
	@Path("/rentbike")
	@Produces(MediaType.APPLICATION_JSON)
	public Bike rentBike(@QueryParam("bikeid") String bikeid, @QueryParam("stationid") String stationid) {
		if(bikeid == null  || bikeid.trim().equals("")) return null;
		try {
			Station station;
			if(stationid != null)
				station = stationDatabase.search(new Station(stationid, null, null)).get(0);
			else station = null;
			ArrayList<Bike> bikes = EcoBikeDatabase.singleton().search(station, new Bike(bikeid, null, null, null, null));
			if(bikes.size() > 0) {
				return stationDatabase.rentBike(station, bikes.get(0));
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	@GET
	@Path("/returnbike")
	@Produces(MediaType.APPLICATION_JSON)
	public Bike returnBike(@QueryParam("bikeid") String bikeid, @QueryParam("stationid") String stationid) {
		if(bikeid == null || stationid == null || bikeid.trim().equals("") || stationid.trim().equals("")) return null;
		try {
			Station station = stationDatabase.search(new Station(stationid, null, null)).get(0);
			ArrayList<Bike> bikes = EcoBikeDatabase.singleton().search(new Bike(bikeid, null, null, null, null));
			if(bikes.size() > 0) {
				return stationDatabase.returnBike(station, bikes.get(0));
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	@POST
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Station updateStation(Station station) {
		return stationDatabase.update(station);
	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Station addStation(Station station) {
		return stationDatabase.add(station);
	}
	
	
	@POST
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Station deleteStation(@PathParam("id") String id) {
		return stationDatabase.delete(new Station(id, null, null));
	}
}
